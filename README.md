# Vulkan over OpenCL
Implementation of Vulkan Compute as an InstanceLayer

# How to run
``` bash
VK_ICD_FILENAMES=./VKOverCL.json ./myApplication
```