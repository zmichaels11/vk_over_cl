#include "volk.h"

#include <stdexcept>
#include <array>
#include <vector>
#include <iostream>

int main(int argc, char** argv) {
    auto err = volkInitialize();

    if (VK_SUCCESS == err) {
        std::cout << "Volk Initialized: TRUE" << std::endl;
    } else {
        throw std::runtime_error("Failed to initialize Volk!");
    }

    VkInstance instance;

    {
        auto applicationI = VkApplicationInfo{};
        applicationI.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        applicationI.pEngineName = "TEST";
        applicationI.pApplicationName = "TEST";
        applicationI.engineVersion = VK_MAKE_VERSION(0, 1, 0);
        applicationI.apiVersion = VK_VERSION_1_0;
        applicationI.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
        
        auto instanceCI = VkInstanceCreateInfo{};
        instanceCI.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        instanceCI.pApplicationInfo = &applicationI;
        
        err = vkCreateInstance(&instanceCI, nullptr, &instance);

        if (VK_SUCCESS == err) {
            std::cout << "Created Vulkan instance!" << std::endl;
        } else {
            throw std::runtime_error("Failed to create Vulkan instance!");
        }

        volkLoadInstance(instance);
    }

    auto gpus = std::vector<VkPhysicalDevice> ();

    {
        uint32_t gpuCount = 0;
        vkEnumeratePhysicalDevices(instance, &gpuCount, nullptr);        

        if (gpuCount > 0) {
            std::cout << "Found " << std::dec << gpuCount << " Vulkan Device(s)!" << std::endl;
        } else {
            throw std::runtime_error("No Vulkan physical devices were found!");
        }

        gpus.resize(gpuCount);

        vkEnumeratePhysicalDevices(instance, &gpuCount, gpus.data());
    }

    VkDevice device;

    {
        auto priorities = std::array<float, 1> ({1.0F});
        auto deviceQueueCI = VkDeviceQueueCreateInfo {};
        deviceQueueCI.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCI.queueFamilyIndex = 0;
        deviceQueueCI.queueCount = priorities.size();
        deviceQueueCI.pQueuePriorities = priorities.data();
        
        auto deviceCI = VkDeviceCreateInfo {};
        deviceCI.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        deviceCI.pQueueCreateInfos = &deviceQueueCI;
        deviceCI.queueCreateInfoCount = 1;
        
        vkCreateDevice(gpus[0], &deviceCI, nullptr, &device);
    }  

    vkDestroyDevice(device, nullptr);
    vkDestroyInstance(instance, nullptr);

    return 0;
}