#include "volk.h"

#include <array>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

struct Buffer {
    VkBuffer handle;
    VmaAllocation memory;
};

int main(int argc, char** argv) {
    auto err = volkInitialize();

    if (VK_SUCCESS != err) {
        throw std::runtime_error("Failed to initialize Volk!");
    }

    VkInstance instance;
    {
        auto applicationI = VkApplicationInfo{};
        applicationI.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        applicationI.pEngineName = "TEST";
        applicationI.pApplicationName = "TEST";
        applicationI.engineVersion = VK_MAKE_VERSION(0, 1, 0);
        applicationI.apiVersion = VK_MAKE_VERSION(1, 0, 0);
        applicationI.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
        
        auto layers = std::vector<const char *>();
        //layers.push_back("VK_LAYER_LUNARG_standard_validation");
        layers.push_back("VK_LAYER_LUNARG_api_dump");

        auto instanceCI = VkInstanceCreateInfo{};
        instanceCI.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        instanceCI.pApplicationInfo = &applicationI;
        instanceCI.ppEnabledLayerNames = layers.data();
        instanceCI.enabledLayerCount = layers.size();
        
        err = vkCreateInstance(&instanceCI, nullptr, &instance);

        if (VK_SUCCESS != err) {
            throw std::runtime_error("Failed to create Vulkan instance!");
        }

        volkLoadInstance(instance);
    }

    auto gpus = std::vector<VkPhysicalDevice> ();
    {
        uint32_t gpuCount = 0;
        vkEnumeratePhysicalDevices(instance, &gpuCount, nullptr);        

        if (gpuCount == 0) {
            throw std::runtime_error("No Vulkan physical devices were found!");
        }

        gpus.resize(gpuCount);

        vkEnumeratePhysicalDevices(instance, &gpuCount, gpus.data());
    }

    uint32_t selectedQueueFamily;
    {
        uint32_t queueFamilyPropertyCount;

        vkGetPhysicalDeviceQueueFamilyProperties(gpus[0], &queueFamilyPropertyCount, nullptr);

        if (0 == queueFamilyPropertyCount) {
            throw std::runtime_error("The PhysicalDevice does not have any QueueFamilies!");
        }

        auto queueFamilies = std::vector<VkQueueFamilyProperties> ();
        queueFamilies.resize(queueFamilyPropertyCount);

        vkGetPhysicalDeviceQueueFamilyProperties(gpus[0], &queueFamilyPropertyCount, queueFamilies.data());

        for (selectedQueueFamily = 0; selectedQueueFamily < queueFamilyPropertyCount; selectedQueueFamily++) {
            if (queueFamilies[selectedQueueFamily].queueFlags & VK_QUEUE_COMPUTE_BIT) {
                // select the first queue that has COMPUTE_BIT
                break;
            }
        }        
    }

    VkDevice device;
    {
        auto priorities = std::array<float, 1> ({1.0F});
        auto deviceQueueCI = VkDeviceQueueCreateInfo {};
        deviceQueueCI.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCI.queueFamilyIndex = selectedQueueFamily;
        deviceQueueCI.queueCount = priorities.size();
        deviceQueueCI.pQueuePriorities = priorities.data();
        
        auto deviceCI = VkDeviceCreateInfo {};
        deviceCI.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        deviceCI.pQueueCreateInfos = &deviceQueueCI;
        deviceCI.queueCreateInfoCount = 1;
        
        vkCreateDevice(gpus[0], &deviceCI, nullptr, &device);
        volkLoadDevice(device);
    }

    VmaAllocator vma;
    {
        auto vmaVulkanFn = VmaVulkanFunctions{};
        vmaVulkanFn.vkAllocateMemory = vkAllocateMemory;
        vmaVulkanFn.vkBindBufferMemory = vkBindBufferMemory;
        vmaVulkanFn.vkBindImageMemory = vkBindImageMemory;
        vmaVulkanFn.vkCmdCopyBuffer = vkCmdCopyBuffer;
        vmaVulkanFn.vkCreateBuffer = vkCreateBuffer;
        vmaVulkanFn.vkCreateImage = vkCreateImage;
        vmaVulkanFn.vkDestroyBuffer = vkDestroyBuffer;
        vmaVulkanFn.vkDestroyImage = vkDestroyImage;
        vmaVulkanFn.vkFlushMappedMemoryRanges = vkFlushMappedMemoryRanges;
        vmaVulkanFn.vkFreeMemory = vkFreeMemory;
        vmaVulkanFn.vkGetBufferMemoryRequirements = vkGetBufferMemoryRequirements;
        vmaVulkanFn.vkGetBufferMemoryRequirements2KHR = vkGetBufferMemoryRequirements2KHR;
        vmaVulkanFn.vkGetImageMemoryRequirements = vkGetImageMemoryRequirements;
        vmaVulkanFn.vkGetImageMemoryRequirements2KHR = vkGetImageMemoryRequirements2KHR;
        vmaVulkanFn.vkGetPhysicalDeviceMemoryProperties = vkGetPhysicalDeviceMemoryProperties;
        vmaVulkanFn.vkGetPhysicalDeviceProperties = vkGetPhysicalDeviceProperties;
        vmaVulkanFn.vkInvalidateMappedMemoryRanges = vkInvalidateMappedMemoryRanges;
        vmaVulkanFn.vkMapMemory = vkMapMemory;
        vmaVulkanFn.vkUnmapMemory = vkUnmapMemory;

        auto vmaCI = VmaAllocatorCreateInfo{};
        vmaCI.physicalDevice = gpus[0];
        vmaCI.device = device;
        vmaCI.pVulkanFunctions = &vmaVulkanFn;

        vmaCreateAllocator(&vmaCI, &vma);
    }

    Buffer inputA;
    {
        auto bufferCI = VkBufferCreateInfo{};
        bufferCI.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCI.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        bufferCI.size = 1024 * sizeof(float);

        auto allocationCI = VmaAllocationCreateInfo{};
        allocationCI.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

        vmaCreateBuffer(vma, &bufferCI, &allocationCI, &inputA.handle, &inputA.memory, nullptr);
    }

    Buffer inputB;
    {
        auto bufferCI = VkBufferCreateInfo{};
        bufferCI.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCI.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        bufferCI.size = 1024 * sizeof(float);

        auto allocationCI = VmaAllocationCreateInfo{};
        allocationCI.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

        vmaCreateBuffer(vma, &bufferCI, &allocationCI, &inputB.handle, &inputB.memory, nullptr);
    }

    Buffer output;
    {
        auto bufferCI = VkBufferCreateInfo{};
        bufferCI.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCI.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        bufferCI.size = 1024 * sizeof(float);

        auto allocationCI = VmaAllocationCreateInfo{};
        allocationCI.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;

        vmaCreateBuffer(vma, &bufferCI, &allocationCI, &output.handle, &output.memory, nullptr);
    }

    VkShaderModule shader;
    {
        //auto file = std::ifstream("src/testAdd/glsl/add.comp.spv", std::ios::binary | std::ios::ate);
        auto file = std::ifstream("src/testAdd/cl/add.cl", std::ios::binary | std::ios::ate);
        auto buffer = std::vector<char>();
        auto size = file.tellg();
        
        buffer.resize(size + 4);
        file.seekg(0, std::ios::beg);

        if (!file.read(buffer.data(), size)) {
            throw std::runtime_error("Failed to read compute shader!");
        }

        buffer.push_back(0);
        buffer.push_back(0);
        buffer.push_back(0);
        buffer.push_back(0);

        auto addCI = VkShaderModuleCreateInfo {};
        addCI.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        addCI.codeSize = buffer.size();
        addCI.pCode = reinterpret_cast<const uint32_t *> (buffer.data());
        
        vkCreateShaderModule(device, &addCI, nullptr, &shader);
    }

    VkDescriptorSetLayout setLayout;
    {
        auto bindings = std::vector<VkDescriptorSetLayoutBinding>();
        {
            auto binding = VkDescriptorSetLayoutBinding{};
            binding.binding = 0;
            binding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
            binding.descriptorCount = 1;
            binding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

            bindings.push_back(binding);

            binding.binding = 1;
            bindings.push_back(binding);

            binding.binding = 2;
            bindings.push_back(binding);
        }

        auto setLayoutCI = VkDescriptorSetLayoutCreateInfo{};
        setLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        setLayoutCI.bindingCount = bindings.size();
        setLayoutCI.pBindings = bindings.data();
        
        vkCreateDescriptorSetLayout(device, &setLayoutCI, nullptr, &setLayout);
    }

    VkPipelineLayout layout;
    {
        auto layoutCI = VkPipelineLayoutCreateInfo{};
        layoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        layoutCI.setLayoutCount = 1;
        layoutCI.pSetLayouts = &setLayout;
        
        vkCreatePipelineLayout(device, &layoutCI, nullptr, &layout);
    }

    VkPipeline pipeline;
    {
        auto pipelineCI = VkComputePipelineCreateInfo {};
        pipelineCI.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
        pipelineCI.stage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        pipelineCI.stage.pName = "main";
        pipelineCI.stage.module = shader;
        pipelineCI.stage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
        pipelineCI.layout = layout;

        vkCreateComputePipelines(device, VK_NULL_HANDLE, 1, &pipelineCI, nullptr, &pipeline);
    }

    VkDescriptorPool descriptorPool;
    {
        auto poolSize = VkDescriptorPoolSize{};
        poolSize.descriptorCount = 50;
        poolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

        auto descriptorPoolCI = VkDescriptorPoolCreateInfo{};
        descriptorPoolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        descriptorPoolCI.maxSets = 50;
        descriptorPoolCI.poolSizeCount = 1;
        descriptorPoolCI.pPoolSizes = &poolSize;

        vkCreateDescriptorPool(device, &descriptorPoolCI, nullptr, &descriptorPool);
    }

    VkDescriptorSet descriptorSet;
    {
        auto descriptorSetAI = VkDescriptorSetAllocateInfo{};
        descriptorSetAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        descriptorSetAI.pSetLayouts = &setLayout;
        descriptorSetAI.descriptorSetCount = 1;
        descriptorSetAI.descriptorPool = descriptorPool;

        vkAllocateDescriptorSets(device, &descriptorSetAI, &descriptorSet);
    }

    {
        auto bufferInfos = std::vector<VkDescriptorBufferInfo>();
        {
            auto bufferInfo = VkDescriptorBufferInfo{};
            bufferInfo.buffer = output.handle;
            bufferInfo.offset = 0;
            bufferInfo.range = VK_WHOLE_SIZE;

            bufferInfos.push_back(bufferInfo);

            bufferInfo.buffer = inputA.handle;
            bufferInfos.push_back(bufferInfo);

            bufferInfo.buffer = inputB.handle;
            bufferInfos.push_back(bufferInfo);
        }

        auto writes = std::vector<VkWriteDescriptorSet>();
        {
            auto write = VkWriteDescriptorSet{};
            write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            write.descriptorCount = 1;
            write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
            write.dstSet = descriptorSet;
            write.pBufferInfo = &bufferInfos[0];
            write.dstBinding = 0;

            writes.push_back(write);

            write.pBufferInfo = &bufferInfos[1];
            write.dstBinding = 1;
            writes.push_back(write);

            write.pBufferInfo = &bufferInfos[2];
            write.dstBinding = 2;
            writes.push_back(write);
        }

        vkUpdateDescriptorSets(device, writes.size(), writes.data(), 0, nullptr);
    }
    
    VkQueue queue;
    
    vkGetDeviceQueue(device, 0, 0, &queue);

    VkCommandPool commandPool;
    {
        auto commandPoolCI = VkCommandPoolCreateInfo{};
        commandPoolCI.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        commandPoolCI.queueFamilyIndex = 0;
        
        vkCreateCommandPool(device, &commandPoolCI, nullptr, &commandPool);
    }

    VkCommandBuffer commandBuffer;
    {
        auto commandBufferAI = VkCommandBufferAllocateInfo{};
        commandBufferAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAI.commandPool = commandPool;
        commandBufferAI.commandBufferCount = 1;

        vkAllocateCommandBuffers(device, &commandBufferAI, &commandBuffer);
    }

    {
        auto commandBufferBI = VkCommandBufferBeginInfo{};
        commandBufferBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        

        vkBeginCommandBuffer(commandBuffer, &commandBufferBI);
    }

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
    vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, layout, 0, 1, &descriptorSet, 0, nullptr);
    vkCmdDispatch(commandBuffer, 4, 1, 1);
    vkEndCommandBuffer(commandBuffer);

    {
        float * pInputA = nullptr;
        float * pInputB = nullptr;

        vmaMapMemory(vma, inputA.memory, reinterpret_cast<void **> (&pInputA));
        vmaMapMemory(vma, inputB.memory, reinterpret_cast<void **> (&pInputB));

        for (int i = 0; i < 1000; i++) {
            pInputA[i] = 1000 - i;
            pInputB[i] = i;
        }

        vmaUnmapMemory(vma, inputB.memory);
        vmaUnmapMemory(vma, inputA.memory);
    }    

    {
        auto submitI = VkSubmitInfo{};
        submitI.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitI.pCommandBuffers = &commandBuffer;
        submitI.commandBufferCount = 1;
        
        vkQueueSubmit(queue, 1, &submitI, VK_NULL_HANDLE);
    }

    vkDeviceWaitIdle(device);

    {
        float * pData = nullptr;

        vmaMapMemory(vma, output.memory, reinterpret_cast<void **> (&pData));

        for (int i = 0; i < 1000; i++) {
            std::cout << pData[i] << ", ";
        }

        std::cout << std::endl;

        vmaUnmapMemory(vma, output.memory);
    }

    vkDestroyCommandPool(device, commandPool, nullptr);
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
    vkDestroyPipeline(device, pipeline, nullptr);
    vkDestroyPipelineLayout(device, layout, nullptr);
    vkDestroyDescriptorSetLayout(device, setLayout, nullptr);
    vkDestroyShaderModule(device, shader, nullptr);

    vmaDestroyBuffer(vma, output.handle, output.memory);
    vmaDestroyBuffer(vma, inputB.handle, inputB.memory);
    vmaDestroyBuffer(vma, inputA.handle, inputA.memory);
    vmaDestroyAllocator(vma);

    vkDestroyDevice(device, nullptr);
    vkDestroyInstance(instance, nullptr);

    return 0;
}