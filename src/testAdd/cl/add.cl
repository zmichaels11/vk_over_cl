__kernel void main(
    __global float* output,
    __global float* inputA,
    __global float* inputB) {

    int i = get_global_id(0);
    
    output[i] = inputA[i] + inputB[i];
}        