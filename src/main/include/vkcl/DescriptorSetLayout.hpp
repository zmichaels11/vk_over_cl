#pragma once

#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class DescriptorSetLayout : public DispatchableObject {
    public:
        static DescriptorSetLayout * getDescriptorSetLayout(VkDescriptorSetLayout vkDescriptorSetLayout) noexcept;

        VkDescriptorSetLayout getVkDescriptorSetLayout() noexcept;
    };
}

#include "vkcl/private/DescriptorSetLayout.inl"
