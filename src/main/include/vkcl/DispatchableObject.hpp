#pragma once

#include <vulkan/vk_icd.h>

namespace vkcl {
    class DispatchableObject {
        struct ICDRef {
            VK_LOADER_DATA loaderData;
            DispatchableObject * object;
        };

        ICDRef _icdRef = { ICD_LOADER_MAGIC, this };

    public:
        static DispatchableObject * getDispatchableObject(void * handle) noexcept;

        void * getVkHandle() noexcept;

        virtual void destroy() noexcept;
    };
}

#include "vkcl/private/DispatchableObject.inl"
