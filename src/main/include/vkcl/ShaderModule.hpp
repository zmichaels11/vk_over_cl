#pragma once

#include <CL/cl.h>
#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class Device;

    class ShaderModule : public DispatchableObject {
        cl_program _program;
        VkShaderModuleCreateInfo _info;

        ShaderModule(const ShaderModule&) = delete;
        ShaderModule& operator= (const ShaderModule&) = delete;

    public:
        static ShaderModule * getShaderModule(VkShaderModule vkShaderModule) noexcept;

        ShaderModule(Device * pDevice, const VkShaderModuleCreateInfo * pInfo);

        virtual ~ShaderModule() noexcept;

        VkShaderModule getVkShaderModule() noexcept;

        const VkShaderModuleCreateInfo& getInfo() const noexcept;

        cl_program getCLProgram() const noexcept;
    };
}

#include "vkcl/private/ShaderModule.inl"
