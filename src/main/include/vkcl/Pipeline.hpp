#pragma once

#include <CL/cl.h>

#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class Device;
    class ShaderModule;
    class Pipeline : public DispatchableObject {
        ShaderModule * _shader;
        cl_kernel _kernel;

    public:
        static Pipeline * getPipeline(VkPipeline vkPipeline) noexcept;

        Pipeline(Device * device, const VkComputePipelineCreateInfo * pInfo);        

        VkPipeline getVkPipeline() noexcept;

        ShaderModule * getShaderModule() const noexcept;

        cl_kernel getCLKernel() const noexcept;
    };
}

#include "vkcl/private/Pipeline.inl"
