#pragma once

#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class DescriptorPool : public DispatchableObject {
    public:
        static DescriptorPool * getDescriptorPool(VkDescriptorPool vkDescriptorPool) noexcept;

        VkDescriptorPool getVkDescriptorPool() noexcept;
    };
}

#include "vkcl/private/DescriptorPool.inl"
