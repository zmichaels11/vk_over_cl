#pragma once

#include <vulkan/vk_icd.h>

namespace vkcl {
    struct CmdBindPipeline {
        VkPipelineBindPoint bindPoint;
        VkPipeline pipeline;
    };

    struct CmdBindDescriptorSets {
        VkPipelineBindPoint bindPoint;
        VkPipelineLayout layout;
        uint32_t firstSet, setCount;
        const VkDescriptorSet * pDescriptorSets;
        uint32_t dynamicOffsetCount;
        const uint32_t * pDynamicOffsets;
    };

    struct CmdDispatch {
        uint32_t x, y, z;
    };

    struct CmdDispatchIndirect {
        VkBuffer buffer;
        VkDeviceSize offset;
    };

    struct CmdCopyBuffer {
        VkBuffer srcBuffer, dstBuffer;
        uint32_t regionCount;
        const VkBufferCopy * pRegions;
    };

    struct CmdUpdateBuffer {
        VkBuffer dstBuffer;
        VkDeviceSize dstOffset, dataSize;
        const void * pData;
    };

    struct CmdFillBuffer {
        VkBuffer dstBuffer;
        VkDeviceSize dstOffset, size;
        uint32_t data;
    };

    struct CmdPipelineBarrier {
        VkPipelineStageFlags srcStage, dstStage;
        VkDependencyFlags dependencyFlags;
        uint32_t memoryBarrierCount;
        const VkMemoryBarrier* pMemoryBarriers;
        uint32_t bufferMemoryBarrierCount;
        const VkBufferMemoryBarrier* pBufferMemoryBarriers; 
        uint32_t imageMemoryBarrierCount;
        const VkImageMemoryBarrier* pImageMemoryBarriers; 
    };

    struct CmdPushConstants {
        VkPipelineLayout layout;
        VkShaderStageFlags stageFlags;
        uint32_t offset;
        uint32_t size;
        const void * pValues;
    };

    struct CmdExecuteCommands {
        uint32_t cmdBufferCount;
        const VkCommandBuffer * pCommandBuffers;
    };
}