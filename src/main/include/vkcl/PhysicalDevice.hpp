#pragma once

#include <CL/cl.h>

#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class Instance;

    class PhysicalDevice : public DispatchableObject {
        Instance * _instance;
        cl_platform_id _platform;
        cl_device_id _device;

        PhysicalDevice(const PhysicalDevice&) = delete;

        PhysicalDevice& operator= (const PhysicalDevice&) = delete;

    public:
        static PhysicalDevice * getPhysicalDevice(VkPhysicalDevice vkPhysicalDevice) noexcept;

        PhysicalDevice(Instance * instance, cl_platform_id platform, cl_device_id device) noexcept:
            _instance(instance),
            _platform(platform),
            _device(device) {}

        virtual ~PhysicalDevice() noexcept;

        VkPhysicalDevice getVkPhysicalDevice() noexcept;

        Instance * getInstance() noexcept;

        void getProperties(VkPhysicalDeviceProperties * pProperties) noexcept;

        void getFeatures(VkPhysicalDeviceFeatures * pFeatures) noexcept;

        void getFormatProperties(VkFormat format, VkFormatProperties * pFormatProperties) noexcept;

        void getQueueFamilyProperties(uint32_t* pQueueFamilyPropertyCount, VkQueueFamilyProperties* pQueueFamilyProperties) noexcept;

        void getMemoryProperties(VkPhysicalDeviceMemoryProperties* pMemoryProperties) noexcept;

        cl_platform_id getCLPlatformID() const noexcept;

        cl_device_id getCLDeviceID() const noexcept;
    };
}

#include "vkcl/private/PhysicalDevice.inl"
