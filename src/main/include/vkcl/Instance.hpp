#pragma once

#include <CL/cl.h>

#include "vkcl/DispatchableObject.hpp"

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace vkcl {
    class PhysicalDevice;

    class Instance : public DispatchableObject {
        std::unordered_map<std::string, PFN_vkVoidFunction> _procAddrMap;
        std::vector<std::unique_ptr<PhysicalDevice>> _physicalDevices;

        void identifyPhysicalDevices() noexcept;
        void identifyInstanceProcs() noexcept;

        Instance(const Instance&) = delete;
        Instance& operator= (const Instance&) = delete;

    public:
        static Instance * getInstance(VkInstance vkInstance) noexcept;

        Instance(const VkInstanceCreateInfo * pCreateInfo) noexcept;

        virtual ~Instance() noexcept;

        VkInstance getVkInstance() noexcept;

        PFN_vkVoidFunction getProcAddr(const char * pName) noexcept;

        VkResult getPhysicalDevices(uint32_t * pPhysicalDeviceCount, VkPhysicalDevice* pPhysicalDevices) noexcept;

        
    };
}

#include "vkcl/private/Instance.inl"
