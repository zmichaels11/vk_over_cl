#pragma once

#include <memory>
#include <vector>

#include <CL/cl.h>

#include "vkcl/DispatchableObject.hpp"
#include "vkcl/Queue.hpp"

namespace vkcl {
    class Instance;
    class PhysicalDevice;
    class Memory;

    class Device : public DispatchableObject {
        PhysicalDevice * _physicalDevice;
        cl_context _context;
        std::vector<std::unique_ptr<Queue>> _queues;
        cl_command_queue _memoryQueue;

        Device(const Device&) = delete;

        Device& operator= (const Device&) = delete;

    public:
        static Device * getDevice(VkDevice vkDevice) noexcept;

        Device(
            PhysicalDevice * physicalDevice,
            const VkDeviceCreateInfo * pCreateInfo);

        virtual ~Device() noexcept;

        VkDevice getVkDevice() noexcept;

        PhysicalDevice * getPhysicalDevice() noexcept;

        PFN_vkVoidFunction getProcAddr(const char * pName) noexcept;

        VkResult waitIdle() noexcept;

        Queue * getQueue(uint32_t queueId) noexcept;

        cl_context getCLContext() const noexcept;

        cl_command_queue getMemoryQueue() const noexcept;
    };
}

#include "vkcl/private/Device.inl"
