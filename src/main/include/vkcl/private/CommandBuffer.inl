namespace vkcl {
    inline CommandBuffer * CommandBuffer::getCommandBuffer(VkCommandBuffer vkCommandBuffer) noexcept {
        return reinterpret_cast<CommandBuffer * > (getDispatchableObject(vkCommandBuffer));
    }

    inline VkCommandBuffer CommandBuffer::getVkCommandBuffer() noexcept {
        return reinterpret_cast<VkCommandBuffer> (getVkHandle());
    }
}