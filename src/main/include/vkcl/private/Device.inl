namespace vkcl {
    inline PhysicalDevice * Device::getPhysicalDevice() noexcept {
        return _physicalDevice;        
    }

    inline Device * Device::getDevice(VkDevice vkDevice) noexcept {
        return reinterpret_cast<Device * > (DispatchableObject::getDispatchableObject(vkDevice));
    }

    inline VkDevice Device::getVkDevice() noexcept {
        return reinterpret_cast<VkDevice> (getVkHandle());
    }

    inline Queue * Device::getQueue(uint32_t queueId) noexcept {
        return _queues[queueId].get();
    }

    inline cl_context Device::getCLContext() const noexcept {
        return _context;
    }

    inline cl_command_queue Device::getMemoryQueue() const noexcept {
        return _memoryQueue;
    }
}
