namespace vkcl {
    inline ShaderModule * ShaderModule::getShaderModule(VkShaderModule vkShaderModule) noexcept {
        return reinterpret_cast<ShaderModule *> (DispatchableObject::getDispatchableObject(vkShaderModule));
    }

    inline VkShaderModule ShaderModule::getVkShaderModule() noexcept {
        return reinterpret_cast<VkShaderModule> (getVkHandle());
    }

    inline cl_program ShaderModule::getCLProgram() const noexcept {
        return _program;
    }
}