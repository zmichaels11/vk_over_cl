namespace vkcl {
    inline Pipeline * Pipeline::getPipeline(VkPipeline vkPipeline) noexcept {
        return reinterpret_cast<Pipeline * > (DispatchableObject::getDispatchableObject(vkPipeline));
    }

    inline VkPipeline Pipeline::getVkPipeline() noexcept {
        return reinterpret_cast<VkPipeline> (getVkHandle());
    }

    inline ShaderModule * Pipeline::getShaderModule() const noexcept {
        return _shader;
    }

    inline cl_kernel Pipeline::getCLKernel() const noexcept {
        return _kernel;
    }
}
