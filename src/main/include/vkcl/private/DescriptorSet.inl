namespace vkcl {
    inline DescriptorSet * DescriptorSet::getDescriptorSet(VkDescriptorSet vkDescriptorSet) noexcept {
        return reinterpret_cast<DescriptorSet * > (DispatchableObject::getDispatchableObject(vkDescriptorSet));
    }

    inline VkDescriptorSet DescriptorSet::getVkDescriptorSet() noexcept {
        return reinterpret_cast<VkDescriptorSet> (getVkHandle());
    }

    inline const std::vector<DescriptorSet::ArgumentWrite>& DescriptorSet::getArguments() const noexcept {
        return _arguments;
    }
}
