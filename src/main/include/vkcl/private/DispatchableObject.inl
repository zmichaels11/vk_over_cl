namespace vkcl {
    inline DispatchableObject * DispatchableObject::getDispatchableObject(void * handle) noexcept {
        if (handle) {
            auto pICDRef = reinterpret_cast<ICDRef * > (handle);

            return pICDRef->object;
        } else {
            return nullptr;
        }
    }

    inline void * DispatchableObject::getVkHandle() noexcept {
        return &_icdRef;
    }

    inline void DispatchableObject::destroy() noexcept {
        delete this;
    }    
}
