namespace vkcl {
    inline PhysicalDevice * PhysicalDevice::getPhysicalDevice(VkPhysicalDevice vkPhysicalDevice) noexcept {
        return reinterpret_cast<PhysicalDevice * > (DispatchableObject::getDispatchableObject(vkPhysicalDevice));
    }

    inline VkPhysicalDevice PhysicalDevice::getVkPhysicalDevice() noexcept {
        return reinterpret_cast<VkPhysicalDevice> (getVkHandle());
    }

    inline Instance * PhysicalDevice::getInstance() noexcept {
        return _instance;
    }

    inline cl_device_id PhysicalDevice::getCLDeviceID() const noexcept {
        return _device;
    }

    inline cl_platform_id PhysicalDevice::getCLPlatformID() const noexcept {
        return _platform;
    }
}
