namespace vkcl {
    inline DescriptorSetLayout * DescriptorSetLayout::getDescriptorSetLayout(VkDescriptorSetLayout vkDescriptorSetLayout) noexcept {
        return reinterpret_cast<DescriptorSetLayout *> (DispatchableObject::getDispatchableObject(vkDescriptorSetLayout));
    }

    inline VkDescriptorSetLayout DescriptorSetLayout::getVkDescriptorSetLayout() noexcept {
        return reinterpret_cast<VkDescriptorSetLayout> (getVkHandle());
    }
}