namespace vkcl {
    inline PipelineLayout * PipelineLayout::getPipelineLayout(VkPipelineLayout vkPipelineLayout) noexcept {
        return reinterpret_cast<PipelineLayout * > (DispatchableObject::getDispatchableObject(vkPipelineLayout));
    }

    inline VkPipelineLayout PipelineLayout::getVkPipelineLayout() noexcept {
        return reinterpret_cast<VkPipelineLayout> (getVkHandle());
    }
}
