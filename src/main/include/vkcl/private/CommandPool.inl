namespace vkcl {
    inline CommandPool * CommandPool::getCommandPool(VkCommandPool vkCommandPool) noexcept {
        return reinterpret_cast<CommandPool *> (getDispatchableObject(vkCommandPool));
    }

    inline VkCommandPool CommandPool::getVkCommandPool() noexcept {
        return reinterpret_cast<VkCommandPool> (getVkHandle());
    }
}
