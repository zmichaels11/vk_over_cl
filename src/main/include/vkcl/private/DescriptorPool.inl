namespace vkcl {
    inline DescriptorPool * DescriptorPool::getDescriptorPool(VkDescriptorPool vkDescriptorPool) noexcept {
        return reinterpret_cast<DescriptorPool *> (DispatchableObject::getDispatchableObject(vkDescriptorPool));
    }

    inline VkDescriptorPool DescriptorPool::getVkDescriptorPool() noexcept {
        return reinterpret_cast<VkDescriptorPool> (getVkHandle());
    }
}