namespace vkcl {
    inline Memory * Memory::getMemory(VkDeviceMemory vkMemory) noexcept {
        return reinterpret_cast<Memory * > (DispatchableObject::getDispatchableObject(vkMemory));
    }

    inline VkDeviceMemory Memory::getVkMemory() noexcept {
        return reinterpret_cast<VkDeviceMemory> (getVkHandle());
    }

    inline cl_mem Memory::getCLMemory() const noexcept {
        return _memory;
    }

    inline const VkMemoryAllocateInfo& Memory::getInfo() const noexcept {
        return _info;
    }

    inline cl_mem_flags Memory::getFlags() const noexcept {
        return _flags;
    }
}