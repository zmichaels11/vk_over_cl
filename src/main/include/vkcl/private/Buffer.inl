namespace vkcl {
    inline Buffer * Buffer::getBuffer(VkBuffer vkBuffer) noexcept {
        return reinterpret_cast<Buffer *> (DispatchableObject::getDispatchableObject(vkBuffer));
    }

    inline VkBuffer Buffer::getVkBuffer() noexcept {
        return reinterpret_cast<VkBuffer> (getVkHandle());
    }
    
    inline cl_mem Buffer::getCLBuffer() const noexcept {
        return _buffer;
    }

    inline const VkBufferCreateInfo& Buffer::getInfo() const noexcept {
        return _info;
    }
}
