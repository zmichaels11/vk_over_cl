namespace vkcl {
    inline Instance * Instance::getInstance(VkInstance vkInstance) noexcept {
        return reinterpret_cast<Instance * > (DispatchableObject::getDispatchableObject(vkInstance));
    }

    inline VkInstance Instance::getVkInstance() noexcept {
        return reinterpret_cast<VkInstance> (getVkHandle());
    }

    inline PFN_vkVoidFunction Instance::getProcAddr(const char * pName) noexcept {
        return _procAddrMap[pName];
    }
}