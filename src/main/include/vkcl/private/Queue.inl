namespace vkcl {
    inline Queue * Queue::getQueue(VkQueue vkQueue) noexcept {
        return reinterpret_cast<Queue *> (DispatchableObject::getDispatchableObject(vkQueue));
    }

    inline VkQueue Queue::getVkQueue() noexcept {
        return reinterpret_cast<VkQueue> (getVkHandle());
    }

    inline cl_command_queue Queue::getCLQueue() const noexcept {
        return _queue;
    }
}
