#pragma once

#include <CL/cl.h>
#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class Device;
    class Memory;

    class Buffer : public DispatchableObject {
        Device * _pDevice;
        cl_mem _buffer;
        bool _isSubBuffer;
        VkBufferCreateInfo _info;

        Buffer(const Buffer&) = delete;
        Buffer& operator= (const Buffer&) = delete;

    public:
        static Buffer * getBuffer(VkBuffer vkBuffer) noexcept;

        Buffer(Device * pDevice, VkBufferCreateInfo info) noexcept:
            _pDevice(pDevice),
            _buffer(nullptr),
            _isSubBuffer(false),
            _info(info) {}

        virtual ~Buffer() noexcept;

        VkBuffer getVkBuffer() noexcept;

        cl_mem getCLBuffer() const noexcept;

        const VkBufferCreateInfo& getInfo() const noexcept;

        void getMemoryRequirements(VkMemoryRequirements * pMemoryRequirements) noexcept;

        VkResult bindMemory(Memory * memory, VkDeviceSize offset);
    };
}

#include "vkcl/private/Buffer.inl"
