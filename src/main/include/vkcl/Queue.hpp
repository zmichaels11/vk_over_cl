#pragma once

#include <CL/cl.h>

#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class Queue : public DispatchableObject {
        cl_command_queue _queue;

    public:
        static Queue * getQueue(VkQueue vkQueue) noexcept;

        VkQueue getVkQueue() noexcept;

        Queue(cl_command_queue queue = nullptr) noexcept:
            _queue(queue) {}

        cl_command_queue getCLQueue() const noexcept;

        VkResult waitIdle() noexcept;
    };
}

#include "vkcl/private/Queue.inl"
