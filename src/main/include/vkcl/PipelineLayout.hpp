#pragma once

#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class PipelineLayout : public DispatchableObject {    
    public:
        static PipelineLayout * getPipelineLayout(VkPipelineLayout vkPipelineLayout) noexcept;

        VkPipelineLayout getVkPipelineLayout() noexcept;
    };
}

#include "vkcl/private/PipelineLayout.inl"
