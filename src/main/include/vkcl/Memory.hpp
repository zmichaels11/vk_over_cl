#pragma once

#include <CL/cl.h>

#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class Device;

    class Memory : public DispatchableObject {
        cl_mem _memory;
        cl_mem_flags _flags;
        VkMemoryAllocateInfo _info;
        
        Memory(const Memory&) = delete;
        Memory& operator= (const Memory&) = delete;

    public:
        void * userPtr;
        
        static Memory * getMemory(VkDeviceMemory vkMemory) noexcept;

        VkDeviceMemory getVkMemory() noexcept;

        Memory(Device * pDevice, const VkMemoryAllocateInfo * pCreateInfo);

        virtual ~Memory() noexcept;

        cl_mem getCLMemory() const noexcept;

        const VkMemoryAllocateInfo& getInfo() const noexcept;

        cl_mem_flags getFlags() const noexcept;
    };    
}

#include "vkcl/private/Memory.inl"
