#pragma once

#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class CommandPool : public DispatchableObject {
    public:
        static CommandPool * getCommandPool(VkCommandPool vkCommandPool) noexcept;

        VkCommandPool getVkCommandPool() noexcept;
    };
}

#include "vkcl/private/CommandPool.inl"
