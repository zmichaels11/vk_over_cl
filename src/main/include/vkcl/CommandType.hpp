#pragma once

#include <cstdint>

namespace vkcl {
    enum class CommandType : std::uint32_t {
        NO_OP = 0,
        BIND_PIPELINE = 1,
        BIND_DESCRIPTOR_SETS,
        DISPATCH,
        DISPATCH_INDIRECT,
        COPY_BUFFER,
        UPDATE_BUFFER,
        FILL_BUFFER,
        PIPELINE_BARRIER,
        PUSH_CONSTANT,
        EXECUTE_COMMANDS
    };
}
