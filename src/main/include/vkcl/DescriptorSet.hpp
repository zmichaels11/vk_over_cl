#pragma once

#include <vector>

#include <CL/cl.h>

#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class DescriptorSet : public DispatchableObject {
    public:
        struct ArgumentWrite {
            cl_uint index;
            std::size_t size;
            const void * value;
        };

    private:
        std::vector<ArgumentWrite> _arguments;

        DescriptorSet(const DescriptorSet&) = delete;
        DescriptorSet& operator=(const DescriptorSet&) = delete;

    public:
        static DescriptorSet * getDescriptorSet(VkDescriptorSet vkDescriptorSet) noexcept;

        DescriptorSet() noexcept {};

        VkDescriptorSet getVkDescriptorSet() noexcept;

        void writeUpdate(const VkWriteDescriptorSet& write) noexcept;

        const std::vector<ArgumentWrite>& getArguments() const noexcept;
    };
}

#include "vkcl/private/DescriptorSet.inl"
