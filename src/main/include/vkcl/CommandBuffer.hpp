#pragma once

#include <memory>
#include <vector>

#include "vkcl/CommandType.hpp"
#include "vkcl/DispatchableObject.hpp"

namespace vkcl {
    class CommandBuffer : public DispatchableObject {
        CommandBuffer(const CommandBuffer&) = delete;
        CommandBuffer& operator=(const CommandBuffer&) = delete;

        std::vector<std::shared_ptr<void>> _payload;
        std::vector<CommandType> _commands;
        bool _recording;

    public:
        static CommandBuffer * getCommandBuffer(VkCommandBuffer vkCommandBuffer) noexcept;

        CommandBuffer() noexcept {};

        VkCommandBuffer getVkCommandBuffer() noexcept;

        VkResult begin(const VkCommandBufferBeginInfo * pInfo) noexcept;

        VkResult end() noexcept;

        VkResult reset(VkCommandBufferResetFlags flags);

        void bindPipeline(VkPipelineBindPoint bindPoint, VkPipeline pipeline) noexcept;

        void bindDescriptorSets(VkPipelineBindPoint bindPoint, VkPipelineLayout layout, uint32_t firstSet, uint32_t setCount, const VkDescriptorSet * pDescriptorSets, uint32_t dynamicOffsetCount, const uint32_t * pDynamicOffsets) noexcept;

        void dispatch(uint32_t x, uint32_t y, uint32_t z) noexcept;

        void dispatchIndirect(VkBuffer buffer, VkDeviceSize offset) noexcept;

        void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, uint32_t regionCount, const VkBufferCopy * pRegions) noexcept;

        void updateBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize dataSize, const void* pData) noexcept;

        void fillBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize size, uint32_t data) noexcept;

        void pipelineBarrier(VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, VkDependencyFlags dependencyFlags, uint32_t memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers, uint32_t bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, uint32_t imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers) noexcept;

        void pushConstants(VkPipelineLayout layout, VkShaderStageFlags stageFlags, uint32_t offset, uint32_t size, const void* pValues) noexcept;

        void executeCommands(uint32_t cmdBuffersCount, const VkCommandBuffer* pCommandBuffers) noexcept;
    };
}

#include "vkcl/private/CommandBuffer.inl"
