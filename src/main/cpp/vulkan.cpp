#include "vkcl/Buffer.hpp"
#include "vkcl/CommandBuffer.hpp"
#include "vkcl/CommandPool.hpp"
#include "vkcl/DescriptorPool.hpp"
#include "vkcl/DescriptorSet.hpp"
#include "vkcl/DescriptorSetLayout.hpp"
#include "vkcl/Device.hpp"
#include "vkcl/Instance.hpp"
#include "vkcl/Memory.hpp"
#include "vkcl/Pipeline.hpp"
#include "vkcl/PipelineLayout.hpp"
#include "vkcl/PhysicalDevice.hpp"
#include "vkcl/Queue.hpp"
#include "vkcl/ShaderModule.hpp"

#include <cstring>
#include <cstdio>

static VkResult logUnimplemented(const char* fn) {
    fprintf(stderr, "[WARNING] Unimplmented: %s\n", fn);
    return VK_ERROR_INCOMPATIBLE_DRIVER;
}

extern "C" {
    VkResult clvkCreateInstance(const VkInstanceCreateInfo * pCreateInfo, const VkAllocationCallbacks * pAllocator, VkInstance * pInstance) {
        auto instance = new vkcl::Instance(pCreateInfo);

        *pInstance = instance->getVkInstance();

        return VK_SUCCESS;
    }

    void clvkDestoryInstance(VkInstance instance, const VkAllocationCallbacks * pAllocator) {
        if (instance) {
            vkcl::Instance::getInstance(instance)->destroy();
        }
    }

    VkResult clvkEnumeratePhysicalDevices(VkInstance instance, uint32_t * pPhysicalDeviceCount, VkPhysicalDevice* pPhysicalDevices) {
        auto clPhysicalDevice = vkcl::Instance::getInstance(instance);
        
        return clPhysicalDevice->getPhysicalDevices(pPhysicalDeviceCount, pPhysicalDevices);
    }

    void clvkGetPhysicalDeviceFeatures(VkPhysicalDevice physicalDevice, VkPhysicalDeviceFeatures * pFeatures) {
        auto clPhysicalDevice = vkcl::PhysicalDevice::getPhysicalDevice(physicalDevice);

        clPhysicalDevice->getFeatures(pFeatures);
    }

    void clvkGetPhysicalDeviceFormatProperties(VkPhysicalDevice physicalDevice, VkFormat format, VkFormatProperties * pFormatProperties) {
        auto clPhysicalDevice = vkcl::PhysicalDevice::getPhysicalDevice(physicalDevice);

        clPhysicalDevice->getFormatProperties(format, pFormatProperties);
    }

    VkResult clvkGetPhysicalDeviceImageFormatProperties(VkPhysicalDevice physicalDevice, VkFormat format, VkImageType type, VkImageTiling tiling, VkImageUsageFlags usage, VkImageCreateFlags flags, VkImageFormatProperties *pImageFormatProperties) {
        return logUnimplemented("vkGetPhysicalDeviceImageFormatProperties");      
    }

    void clvkGetPhysicalDeviceProperties(VkPhysicalDevice physicalDevice, VkPhysicalDeviceProperties* pProperties) {
        auto clPhysicalDevice = vkcl::PhysicalDevice::getPhysicalDevice(physicalDevice);

        clPhysicalDevice->getProperties(pProperties);
    }

    void clvkGetPhysicalDeviceQueueFamilyProperties(VkPhysicalDevice physicalDevice, uint32_t* pQueueFamilyPropertyCount, VkQueueFamilyProperties* pQueueFamilyProperties) { 
        auto clPhysicalDevice = vkcl::PhysicalDevice::getPhysicalDevice(physicalDevice);

        clPhysicalDevice->getQueueFamilyProperties(pQueueFamilyPropertyCount, pQueueFamilyProperties);
    }

    void clvkGetPhysicalDeviceMemoryProperties(VkPhysicalDevice physicalDevice, VkPhysicalDeviceMemoryProperties* pMemoryProperties) {
        auto clPhysicalDevice = vkcl::PhysicalDevice::getPhysicalDevice(physicalDevice);

        clPhysicalDevice->getMemoryProperties(pMemoryProperties);
    }

    PFN_vkVoidFunction clvkGetDeviceProcAddr(VkDevice device, const char* pName) {
        auto clPhysicalDevice = vkcl::Device::getDevice(device);
        
        return clPhysicalDevice->getPhysicalDevice()->getInstance()->getProcAddr(pName);
    }

    VkResult clvkCreateDevice(
        VkPhysicalDevice physicalDevice, 
        const VkDeviceCreateInfo* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkDevice* pDevice) {

        auto clPhysicalDevice = vkcl::PhysicalDevice::getPhysicalDevice(physicalDevice);
        
        try {
            auto clDevice = new vkcl::Device(clPhysicalDevice, pCreateInfo);

            *pDevice = clDevice->getVkDevice();
        } catch (VkResult err) {
            return err;
        }

        return VK_SUCCESS;
    }

    void clvkDestroyDevice(
        VkDevice device, 
        const VkAllocationCallbacks* pAllocator) {
        
        vkcl::Device::getDevice(device)->destroy();
    }

    VkResult clvkEnumerateInstanceExtensionProperties(const char* pLayerName, uint32_t* pCount, VkExtensionProperties* pProperties) {
        *pCount = 0;
        return VK_SUCCESS;
    }

    VkResult clvkEnumerateDeviceExtensionProperties(VkPhysicalDevice physicalDevice, const char* pLayerName, uint32_t* pCount, VkExtensionProperties* pProperties) {
        *pCount = 0;
        return VK_SUCCESS;
    }

    VkResult clvkEnumerateInstanceLayerProperties(uint32_t* pCount, VkLayerProperties* pProperties) {
        *pCount = 0;
        return VK_SUCCESS;
    }

    VkResult clvkEnumerateDeviceLayerProperties(VkPhysicalDevice physicalDevice, uint32_t* pCount, VkLayerProperties* pProperties) {
        *pCount = 0;
        return VK_SUCCESS;
    }

    void clvkGetDeviceQueue(
        VkDevice device, 
        uint32_t queueFamilyIndex, 
        uint32_t queueIndex, 
        VkQueue* pQueue) {

        auto clDevice = vkcl::Device::getDevice(device);
        auto clQueue = clDevice->getQueue(queueIndex);

        *pQueue = clQueue->getVkQueue();
    }

    VkResult clvkQueueSubmit(
        VkQueue queue, 
        uint32_t submitCount, 
        const VkSubmitInfo* pSubmits, 
        VkFence fence) {

	    return logUnimplemented("vkQueueSubmit");
    }

    VkResult clvkQueueWaitIdle(VkQueue queue) {
        auto clQueue = vkcl::Queue::getQueue(queue);

        return clQueue->waitIdle();
    }

    VkResult clvkDeviceWaitIdle(VkDevice device) {
        auto clDevice = vkcl::Device::getDevice(device);
        
        return clDevice->waitIdle();
    }

    VkResult clvkAllocateMemory(
        VkDevice device, 
        const VkMemoryAllocateInfo* pAllocateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkDeviceMemory* pMem) {

        auto clDevice = vkcl::Device::getDevice(device);
        
        try {
            auto clMemory = new vkcl::Memory(clDevice, pAllocateInfo);

            *pMem = clMemory->getVkMemory();
        } catch (VkResult err) {
            return err;
        }
        
        return VK_SUCCESS;
    }

    void clvkFreeMemory(
        VkDevice device, 
        VkDeviceMemory mem, 
        const VkAllocationCallbacks* pAllocator) {    
            
        auto clMem = vkcl::Memory::getMemory(mem);

        clMem->destroy();
    }

    VkResult clvkMapMemory(
        VkDevice device, 
        VkDeviceMemory mem, 
        VkDeviceSize offset, 
        VkDeviceSize size, 
        VkMemoryMapFlags flags, 
        void** ppData) {


        auto clDevice = vkcl::Device::getDevice(device);
        auto clMemory = vkcl::Memory::getMemory(mem);
        
        if (VK_WHOLE_SIZE == size) {
            size = clMemory->getInfo().allocationSize;
        }

        cl_int err;

        clMemory->userPtr = clEnqueueMapBuffer(
            clDevice->getMemoryQueue(), 
            clMemory->getCLMemory(), 
            CL_TRUE, 
            CL_MAP_READ | CL_MAP_WRITE, 
            offset, 
            size, 
            0, nullptr, nullptr,
            &err);
            
        if (CL_SUCCESS == err) {
            *ppData = clMemory->userPtr;
            return VK_SUCCESS;
        } else {
            return VK_ERROR_MEMORY_MAP_FAILED;
        }
    }

    void clvkUnmapMemory(VkDevice device, VkDeviceMemory mem) {   	
        auto clDevice = vkcl::Device::getDevice(device);
        auto clMemory = vkcl::Memory::getMemory(mem);
        cl_int err = clEnqueueUnmapMemObject(
            clDevice->getMemoryQueue(), 
            clMemory->getCLMemory(),
            clMemory->userPtr,
            0, nullptr, nullptr);

        if (CL_SUCCESS != err) {
            std::printf("Failed to unmap Memory Object!");
        }
    }

    VkResult clvkFlushMappedMemoryRanges(VkDevice device, uint32_t memRangeCount, const VkMappedMemoryRange* pMemRanges) {
        return logUnimplemented("vkFlushMappedMemoryRanges");
    }

    VkResult clvkInvalidateMappedMemoryRanges(VkDevice device, uint32_t memRangeCount, const VkMappedMemoryRange* pMemRanges) {
        return logUnimplemented("vkInvalidateMappedMemoryRanges");
    }

    void clvkGetDeviceMemoryCommitment(VkDevice device, VkDeviceMemory memory, VkDeviceSize* pCommittedMemoryInBytes) {
        logUnimplemented("vkGetDeviceMemoryCommitment");
    }

    VkResult clvkBindBufferMemory(
        VkDevice device, 
        VkBuffer buffer, 
        VkDeviceMemory memory, 
        VkDeviceSize memOffset) {

        auto clMemory = vkcl::Memory::getMemory(memory);
        auto clBuffer = vkcl::Buffer::getBuffer(buffer);

        return clBuffer->bindMemory(clMemory, memOffset);
    }

    VkResult clvkBindImageMemory(VkDevice device, VkImage image, VkDeviceMemory mem, VkDeviceSize memOffset) {	
        return logUnimplemented("vkBindImageMemory");	
    }

    void clvkGetBufferMemoryRequirements(
        VkDevice device, 
        VkBuffer buffer, 
        VkMemoryRequirements* pMemoryRequirements) {

        auto clBuffer = vkcl::Buffer::getBuffer(buffer);

        clBuffer->getMemoryRequirements(pMemoryRequirements);
    }

    void clvkGetImageMemoryRequirements(VkDevice device, VkImage image, VkMemoryRequirements* pMemoryRequirements) {    
        logUnimplemented("vkGetImageMemoryRequirements");
    }

    void clvkGetImageSparseMemoryRequirements(VkDevice device, VkImage image, uint32_t* pNumRequirements, VkSparseImageMemoryRequirements* pSparseMemoryRequirements) {        	
        logUnimplemented("vkGetImageSparseMemoryRequirements");
    }

    void clvkGetPhysicalDeviceSparseImageFormatProperties(VkPhysicalDevice physicalDevice, VkFormat format, VkImageType type, VkSampleCountFlagBits samples, VkImageUsageFlags usage, VkImageTiling tiling, uint32_t* pPropertyCount, VkSparseImageFormatProperties* pProperties) { 
        logUnimplemented("vkGetPhysicalDeviceSparseImageFormatProperties");
    }

    VkResult clvkQueueBindSparse(VkQueue queue, uint32_t bindInfoCount, const VkBindSparseInfo* pBindInfo, VkFence fence) {
        return logUnimplemented("vkQueueBindSparse");
    }

    VkResult clvkCreateFence(VkDevice device, const VkFenceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkFence* pFence) {
        return logUnimplemented("vkCreateFence");
    }

    void clvkDestroyFence(VkDevice device, VkFence fence, const VkAllocationCallbacks* pAllocator) {
        logUnimplemented("vkDestroyFence");
    }

    VkResult clvkResetFences(VkDevice device, uint32_t fenceCount, const VkFence* pFences) {
        return logUnimplemented("vkResetFence");        
    }

    VkResult clvkGetFenceStatus(VkDevice device, VkFence fence) {
        return logUnimplemented("vkGetFenceStatus");        
    }

    VkResult clvkWaitForFences(VkDevice device, uint32_t fenceCount, const VkFence* pFences, VkBool32 waitAll, uint64_t timeout) {
        return logUnimplemented("vkWaitForFences");        
    }

    VkResult clvkCreateSemaphore(VkDevice device, const VkSemaphoreCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSemaphore* pSemaphore) {
	    return logUnimplemented("vkCreateSemaphore");
    }

    void clvkDestroySemaphore(VkDevice device, VkSemaphore semaphore, const VkAllocationCallbacks* pAllocator) {        
        logUnimplemented("vkDestroySemaphore");
    }

    VkResult clvkCreateEvent(VkDevice device, const VkEventCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkEvent* pEvent) {
        return VK_ERROR_INCOMPATIBLE_DRIVER;
    }

    void clvkDestroyEvent(VkDevice device, VkEvent event, const VkAllocationCallbacks* pAllocator) {
    }

    VkResult clvkGetEventStatus(VkDevice device, VkEvent event) {
	    return logUnimplemented("vkGetEventStatus");
    }

    VkResult clvkSetEvent(VkDevice device, VkEvent event) {
	    return logUnimplemented("vkSetEvent");	
    }

    VkResult clvkResetEvent(VkDevice device, VkEvent event) {
	    return logUnimplemented("vkResetEvent");	
    }

    VkResult clvkCreateQueryPool(VkDevice device, const VkQueryPoolCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkQueryPool* pQueryPool) {
	    return logUnimplemented("vkCreateQueryPool");
    }

    void clvkDestroyQueryPool(VkDevice device, VkQueryPool queryPool, const VkAllocationCallbacks* pAllocator) {
        logUnimplemented("vkDestroyQueryPool");
    }

    VkResult clvkGetQueryPoolResults(VkDevice device, VkQueryPool queryPool, uint32_t firstQuery, uint32_t queryCount, size_t dataSize, void* pData, VkDeviceSize stride, VkQueryResultFlags flags) {
	    return logUnimplemented("vkGetQueryPoolResults");
    }

    VkResult clvkCreateBuffer(
        VkDevice device, 
        const VkBufferCreateInfo* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkBuffer* pBuffer) {

        auto clDevice = vkcl::Device::getDevice(device);
        auto clBuffer = new vkcl::Buffer(clDevice, *pCreateInfo);
        
        *pBuffer = clBuffer->getVkBuffer();

	    return VK_SUCCESS;
    }

    void clvkDestroyBuffer(
        VkDevice device, 
        VkBuffer buffer, 
        const VkAllocationCallbacks* pAllocator) {

        auto clBuffer = vkcl::Buffer::getBuffer(buffer);

        clBuffer->destroy();
    }

    VkResult clvkCreateBufferView(VkDevice device, const VkBufferViewCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkBufferView* pView) { 
	    return logUnimplemented("vkCreateBufferView");
    }

    void clvkDestroyBufferView(VkDevice device, VkBufferView bufferView, const VkAllocationCallbacks* pAllocator) {
        logUnimplemented("vkDestroyBufferView");
    }

    VkResult clvkCreateImage(VkDevice device, const VkImageCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkImage* pImage) {
	    return logUnimplemented("vkCreateImage");
    }

    void clvkDestroyImage(VkDevice device, VkImage image, const VkAllocationCallbacks* pAllocator) {
        logUnimplemented("vkDestoryImage");
    }

    void clvkGetImageSubresourceLayout(VkDevice device, VkImage image, const VkImageSubresource* pSubresource, VkSubresourceLayout* pLayout) {
        logUnimplemented("vkGetImageSubresourceLayout");
    }

    VkResult clvkCreateImageView(VkDevice device, const VkImageViewCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkImageView* pView) {
	    return logUnimplemented("vkCreateImageView");
    }

    void clvkDestroyImageView(VkDevice device, VkImageView imageView, const VkAllocationCallbacks* pAllocator) {
        logUnimplemented("vkDestroyImageView");
    }

    VkResult clvkCreateShaderModule(
        VkDevice device, 
        const VkShaderModuleCreateInfo* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkShaderModule* pShaderModule) {

        try {
            auto clDevice = vkcl::Device::getDevice(device);
            auto clShaderModule = new vkcl::ShaderModule(clDevice, pCreateInfo);

            *pShaderModule = clShaderModule->getVkShaderModule();

            return VK_SUCCESS;
        } catch (VkResult err) {
            return err;
        }
    }

    void clvkDestroyShaderModule(
        VkDevice device, 
        VkShaderModule shaderModule, 
        const VkAllocationCallbacks* pAllocator) {

        vkcl::ShaderModule::getShaderModule(shaderModule)->destroy();
    }

    VkResult clvkCreatePipelineCache(VkDevice device, const VkPipelineCacheCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkPipelineCache* pPipelineCache) {
	    return VK_ERROR_INCOMPATIBLE_DRIVER;
    }

    void clvkDestroyPipelineCache(VkDevice device, VkPipelineCache pipelineCache, const VkAllocationCallbacks* pAllocator) {
    }

    VkResult clvkGetPipelineCacheData(VkDevice device, VkPipelineCache pipelineCache, size_t* pDataSize, void* pData) {
	    return VK_ERROR_INCOMPATIBLE_DRIVER;
    }

    VkResult clvkMergePipelineCaches(VkDevice device, VkPipelineCache destCache, uint32_t srcCacheCount, const VkPipelineCache* pSrcCaches) {
	    return VK_ERROR_INCOMPATIBLE_DRIVER;
    }

    VkResult clvkCreateGraphicsPipelines(VkDevice device, VkPipelineCache pipelineCache, uint32_t count, const VkGraphicsPipelineCreateInfo* pCreateInfos, const VkAllocationCallbacks* pAllocator, VkPipeline* pPipelines) {
	    return VK_ERROR_INCOMPATIBLE_DRIVER;
    }

    VkResult clvkCreateComputePipelines(
        VkDevice device, 
        VkPipelineCache pipelineCache, 
        uint32_t count, const VkComputePipelineCreateInfo* pCreateInfos, 
        const VkAllocationCallbacks* pAllocator, VkPipeline* pPipelines) {

        auto clDevice = vkcl::Device::getDevice(device);

        try {
            for (uint32_t i = 0; i < count; i++) {
                auto clProgram = new vkcl::Pipeline(clDevice, pCreateInfos + i);
                
                pPipelines[i] = clProgram->getVkPipeline();
            }
        } catch (VkResult err) {
            return err;
        }

	    return VK_SUCCESS;
    }

    void clvkDestroyPipeline(
        VkDevice device, 
        VkPipeline pipeline, 
        const VkAllocationCallbacks* pAllocator) {

        vkcl::Pipeline::getPipeline(pipeline)->destroy();
    }

    VkResult clvkCreatePipelineLayout(
        VkDevice device, 
        const VkPipelineLayoutCreateInfo* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkPipelineLayout* pPipelineLayout) {

	    auto clPipelineLayout = new vkcl::PipelineLayout();
        *pPipelineLayout = clPipelineLayout->getVkPipelineLayout();

        return VK_SUCCESS;
    }

    void clvkDestroyPipelineLayout(
        VkDevice device, 
        VkPipelineLayout pipelineLayout, 
        const VkAllocationCallbacks* pAllocator) {

        vkcl::PipelineLayout::getPipelineLayout(pipelineLayout)->destroy();
    }

    VkResult clvkCreateSampler(VkDevice device, const VkSamplerCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSampler* pSampler) {
	    return VK_ERROR_INCOMPATIBLE_DRIVER;
    }

    void clvkDestroySampler(VkDevice device, VkSampler sampler, const VkAllocationCallbacks* pAllocator) {	
    }

    VkResult clvkCreateDescriptorSetLayout(
        VkDevice device, 
        const VkDescriptorSetLayoutCreateInfo* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkDescriptorSetLayout* pSetLayout) {

        auto clDescriptorSetLayout = new vkcl::DescriptorSetLayout();
        *pSetLayout = clDescriptorSetLayout->getVkDescriptorSetLayout();

	    return VK_SUCCESS;
    }

    void clvkDestroyDescriptorSetLayout(
        VkDevice device, 
        VkDescriptorSetLayout descriptorSetLayout, 
        const VkAllocationCallbacks* pAllocator) {

        vkcl::DescriptorSetLayout::getDescriptorSetLayout(descriptorSetLayout)->destroy();
    }

    VkResult clvkCreateDescriptorPool(
        VkDevice device, 
        const VkDescriptorPoolCreateInfo* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkDescriptorPool* pDescriptorPool) {
            
        auto clDescriptorPool = new vkcl::DescriptorPool();
        *pDescriptorPool = clDescriptorPool->getVkDescriptorPool();

        return VK_SUCCESS;
    }

    void clvkDestroyDescriptorPool(
        VkDevice device, 
        VkDescriptorPool descriptorPool, 
        const VkAllocationCallbacks* pAllocator) {

        vkcl::DescriptorPool::getDescriptorPool(descriptorPool)->destroy();
    }

    VkResult clvkResetDescriptorPool(VkDevice device, VkDescriptorPool descriptorPool, VkDescriptorPoolResetFlags flags) {
	    return VK_ERROR_INCOMPATIBLE_DRIVER;
    }

    VkResult clvkAllocateDescriptorSets(
        VkDevice device, 
        const VkDescriptorSetAllocateInfo* pAllocateInfo, 
        VkDescriptorSet* pDescriptorSets) {

        for (uint32_t i = 0; i < pAllocateInfo->descriptorSetCount; i++) {
            auto clDescriptorSet = new vkcl::DescriptorSet();

            pDescriptorSets[i] = clDescriptorSet->getVkDescriptorSet();
        }

        return VK_SUCCESS;
    }

    VkResult clvkFreeDescriptorSets(
        VkDevice device, 
        VkDescriptorPool descriptorPool, 
        uint32_t count, 
        const VkDescriptorSet* pDescriptorSets) {
	    
        for (uint32_t i = 0; i < count; i++) {
            auto clDescriptorSet = vkcl::DescriptorSet::getDescriptorSet(pDescriptorSets[i]);

            clDescriptorSet->destroy();
        }
    }

    void clvkUpdateDescriptorSets(
        VkDevice device, 
        uint32_t writeCount, const VkWriteDescriptorSet* pDescriptorWrites, 
        uint32_t copyCount, const VkCopyDescriptorSet* pDescriptorCopies) {

        for (uint32_t i = 0; i < writeCount; i++) {
            auto clDescriptorSet = vkcl::DescriptorSet::getDescriptorSet(pDescriptorWrites[i].dstSet);

            clDescriptorSet->writeUpdate(pDescriptorWrites[i]);
        }
    }

    VkResult clvkCreateFramebuffer(VkDevice device, const VkFramebufferCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkFramebuffer* pFramebuffer) {
	    return VK_ERROR_INCOMPATIBLE_DRIVER;        
    }

    void clvkDestroyFramebuffer(VkDevice device, VkFramebuffer framebuffer, const VkAllocationCallbacks* pAllocator) {

    }

    VkResult clvkCreateRenderPass(VkDevice device, const VkRenderPassCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkRenderPass* pRenderPass) { 
	    return VK_ERROR_INCOMPATIBLE_DRIVER;        
    }

    void clvkDestroyRenderPass(VkDevice device, VkRenderPass renderPass, const VkAllocationCallbacks* pAllocator) {
    }

    void clvkGetRenderAreaGranularity(VkDevice device, VkRenderPass renderPass, VkExtent2D* pGranularity) {

    }

    VkResult clvkCreateCommandPool(
        VkDevice device, 
        const VkCommandPoolCreateInfo* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkCommandPool* pCmdPool) {

        auto clCommandPool = new vkcl::CommandPool();
        *pCmdPool = clCommandPool->getVkCommandPool();

	    return VK_SUCCESS;
    }

    void clvkDestroyCommandPool(
        VkDevice device, 
        VkCommandPool commandPool, 
        const VkAllocationCallbacks* pAllocator) {

        vkcl::CommandPool::getCommandPool(commandPool)->destroy();
    }

    VkResult clvkResetCommandPool(VkDevice device, VkCommandPool commandPool, VkCommandPoolResetFlags flags) {
	return VK_ERROR_INCOMPATIBLE_DRIVER;        
    }

    VkResult clvkAllocateCommandBuffers(
        VkDevice device, 
        const VkCommandBufferAllocateInfo* pAllocateInfo, 
        VkCommandBuffer* pCmdBuffer) {

        for (uint32_t i = 0; i < pAllocateInfo->commandBufferCount; i++) {
            auto clCommandBuffer = new vkcl::CommandBuffer();
            pCmdBuffer[i] = clCommandBuffer->getVkCommandBuffer();
        }

	    return VK_SUCCESS;
    }

    void clvkFreeCommandBuffers(
        VkDevice device, 
        VkCommandPool commandPool, 
        uint32_t commandBufferCount, 
        const VkCommandBuffer* pCommandBuffers) {

        for (uint32_t i = 0; i < commandBufferCount; i++) {
            auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(pCommandBuffers[i]);
            clCommandBuffer->destroy();
        }
    }

    VkResult clvkBeginCommandBuffer(
        VkCommandBuffer commandBuffer, 
        const VkCommandBufferBeginInfo* pBeginInfo) {

	    auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        return clCommandBuffer->begin(pBeginInfo);
    }

    VkResult clvkEndCommandBuffer(VkCommandBuffer commandBuffer) {
	    auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        return clCommandBuffer->end();
    }

    VkResult clvkResetCommandBuffer(VkCommandBuffer commandBuffer, VkCommandBufferResetFlags flags) {
	    auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        return clCommandBuffer->reset(flags);
    }

    void clvkCmdBindPipeline(
        VkCommandBuffer commandBuffer, 
        VkPipelineBindPoint pipelineBindPoint, 
        VkPipeline pipeline) {

        auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        clCommandBuffer->bindPipeline(pipelineBindPoint, pipeline);
    }

    void clvkCmdSetViewport(VkCommandBuffer commandBuffer, uint32_t firstViewport, uint32_t viewportCount, const VkViewport* pViewports) {
    }

    void clvkCmdSetScissor(VkCommandBuffer commandBuffer, uint32_t firstScissor, uint32_t scissorCount, const VkRect2D* pScissors) {
    }

    void clvkCmdSetLineWidth(VkCommandBuffer commandBuffer, float lineWidth) {
    }

    void clvkCmdSetDepthBias(VkCommandBuffer commandBuffer, float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor) {
    }

    void clvkCmdSetBlendConstants(VkCommandBuffer commandBuffer, const float blendConst[4]) {
    }

    void clvkCmdSetDepthBounds(VkCommandBuffer commandBuffer, float minDepthBounds, float maxDepthBounds) {
    }

    void clvkCmdSetStencilCompareMask(VkCommandBuffer commandBuffer, VkStencilFaceFlags faceMask, uint32_t stencilCompareMask) {
    }

    void clvkCmdSetStencilWriteMask(VkCommandBuffer commandBuffer, VkStencilFaceFlags faceMask, uint32_t stencilWriteMask) {
    }

    void clvkCmdSetStencilReference(VkCommandBuffer commandBuffer, VkStencilFaceFlags faceMask, uint32_t stencilReference) {
    }

    void clvkCmdBindDescriptorSets(
        VkCommandBuffer commandBuffer, 
        VkPipelineBindPoint pipelineBindPoint, 
        VkPipelineLayout layout, 
        uint32_t firstSet, uint32_t setCount, const VkDescriptorSet* pDescriptorSets, 
        uint32_t dynamicOffsetCount, const uint32_t* pDynamicOffsets) {

        auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        clCommandBuffer->bindDescriptorSets(pipelineBindPoint, layout, firstSet, setCount, pDescriptorSets, dynamicOffsetCount, pDynamicOffsets);
    }

    void clvkCmdBindIndexBuffer(VkCommandBuffer commandBuffer, VkBuffer buffer, VkDeviceSize offset, VkIndexType indexType) {
    }

    void clvkCmdBindVertexBuffers(VkCommandBuffer commandBuffer, uint32_t startBinding, uint32_t bindingCount, const VkBuffer* pBuffers, const VkDeviceSize* pOffsets) {
    }

    void clvkCmdDraw(VkCommandBuffer commandBuffer, uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance) { 
    }

    void clvkCmdDrawIndexed(VkCommandBuffer commandBuffer, uint32_t indexCount, uint32_t instanceCount, uint32_t firstIndex, int32_t vertexOffset, uint32_t firstInstance) {
    }

    void clvkCmdDrawIndirect(VkCommandBuffer commandBuffer, VkBuffer buffer, VkDeviceSize offset, uint32_t drawCount, uint32_t stride) {
    }

    void clvkCmdDrawIndexedIndirect(VkCommandBuffer commandBuffer, VkBuffer buffer, VkDeviceSize offset, uint32_t drawCount, uint32_t stride) {
    }

    void clvkCmdDispatch(
        VkCommandBuffer commandBuffer, 
        uint32_t x, uint32_t y, uint32_t z) {

        auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        clCommandBuffer->dispatch(x, y, z);
    }

    void clvkCmdDispatchIndirect(
        VkCommandBuffer commandBuffer, 
        VkBuffer buffer, 
        VkDeviceSize offset) {

        auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        clCommandBuffer->dispatchIndirect(buffer, offset);
    }

    void clvkCmdCopyBuffer(
        VkCommandBuffer commandBuffer, 
        VkBuffer srcBuffer, VkBuffer destBuffer, 
        uint32_t regionCount, const VkBufferCopy* pRegions) {

        auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        clCommandBuffer->copyBuffer(srcBuffer, destBuffer, regionCount, pRegions);
    }

    void clvkCmdCopyImage(VkCommandBuffer commandBuffer, VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32_t regionCount, const VkImageCopy* pRegions) { 
    }

    void clvkCmdBlitImage(VkCommandBuffer commandBuffer, VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32_t regionCount, const VkImageBlit* pRegions, VkFilter filter) {
    }

    void clvkCmdCopyBufferToImage(VkCommandBuffer commandBuffer, VkBuffer srcBuffer, VkImage dstImage, VkImageLayout dstImageLayout, uint32_t regionCount, const VkBufferImageCopy* pRegions) {
    }

    void clvkCmdCopyImageToBuffer(VkCommandBuffer commandBuffer, VkImage srcImage, VkImageLayout srcImageLayout, VkBuffer dstBuffer, uint32_t regionCount, const VkBufferImageCopy* pRegions) {
    }

    void clvkCmdUpdateBuffer(
        VkCommandBuffer commandBuffer, 
        VkBuffer dstBuffer, 
        VkDeviceSize dstOffset, VkDeviceSize dataSize, const void* pData) {

        auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        clCommandBuffer->updateBuffer(dstBuffer, dstOffset, dataSize, pData);
    }

    void clvkCmdFillBuffer(
        VkCommandBuffer commandBuffer, 
        VkBuffer dstBuffer, 
        VkDeviceSize dstOffset, VkDeviceSize size, uint32_t data) {

        auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        clCommandBuffer->fillBuffer(dstBuffer, dstOffset, size, data);
    }

    void clvkCmdClearColorImage(VkCommandBuffer commandBuffer, VkImage image, VkImageLayout imageLayout, const VkClearColorValue* pColor, uint32_t rangeCount, const VkImageSubresourceRange* pRanges) {
    }

    void clvkCmdClearDepthStencilImage(VkCommandBuffer commandBuffer, VkImage image, VkImageLayout imageLayout, const VkClearDepthStencilValue* pDepthStencil, uint32_t rangeCount, const VkImageSubresourceRange* pRanges) {
    }

    void clvkCmdClearAttachments(VkCommandBuffer commandBuffer, uint32_t attachmentCount, const VkClearAttachment* pAttachments, uint32_t rectCount, const VkClearRect* pRects) {
    }

    void clvkCmdResolveImage(VkCommandBuffer commandBuffer, VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32_t regionCount, const VkImageResolve* pRegions) { 
    }

    void clvkCmdSetEvent(VkCommandBuffer commandBuffer, VkEvent event, VkPipelineStageFlags stageMask) {
    }

    void clvkCmdResetEvent(VkCommandBuffer commandBuffer, VkEvent event, VkPipelineStageFlags stageMask) {
    }

    void clvkCmdWaitEvents(VkCommandBuffer commandBuffer, uint32_t eventCount, const VkEvent* pEvents, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, uint32_t memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers, uint32_t bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, uint32_t imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers) { 
    }

    void clvkCmdPipelineBarrier(
        VkCommandBuffer commandBuffer, 
        VkPipelineStageFlags srcStageMask, 
        VkPipelineStageFlags dstStageMask, 
        VkDependencyFlags dependencyFlags, 
        uint32_t memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers, 
        uint32_t bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, 
        uint32_t imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers) {

        auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        clCommandBuffer->pipelineBarrier(srcStageMask, dstStageMask, dependencyFlags, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);
    }

    void clvkCmdBeginQuery(VkCommandBuffer commandBuffer, VkQueryPool queryPool, uint32_t query, VkQueryControlFlags flags) {
    }

    void clvkCmdEndQuery(VkCommandBuffer commandBuffer, VkQueryPool queryPool, uint32_t query) {
    }

    void clvkCmdResetQueryPool(VkCommandBuffer commandBuffer, VkQueryPool queryPool, uint32_t firstQuery, uint32_t queryCount) {
    }

    void clvkCmdWriteTimestamp(VkCommandBuffer commandBuffer, VkPipelineStageFlagBits pipelineStage, VkQueryPool queryPool, uint32_t query) {
    }

    void clvkCmdCopyQueryPoolResults(VkCommandBuffer commandBuffer, VkQueryPool queryPool, uint32_t firstQuery, uint32_t queryCount, VkBuffer destBuffer, VkDeviceSize destOffset, VkDeviceSize destStride, VkQueryResultFlags flags) {
    }

    void clvkCmdPushConstants(
        VkCommandBuffer commandBuffer, 
        VkPipelineLayout layout, 
        VkShaderStageFlags stageFlags, 
        uint32_t offset, uint32_t size, const void* pValues) { 

        auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        clCommandBuffer->pushConstants(layout, stageFlags, offset, size, pValues);
    }

    void clvkCmdBeginRenderPass(VkCommandBuffer commandBuffer, const VkRenderPassBeginInfo* pRenderPassBegin, VkSubpassContents contents) {
    }

    void clvkCmdNextSubpass(VkCommandBuffer commandBuffer, VkSubpassContents contents) {
    }

    void clvkCmdEndRenderPass(VkCommandBuffer commandBuffer) { 
    }

    void clvkCmdExecuteCommands(
        VkCommandBuffer commandBuffer, 
        uint32_t cmdBuffersCount, 
        const VkCommandBuffer* pCommandBuffers) {

        auto clCommandBuffer = vkcl::CommandBuffer::getCommandBuffer(commandBuffer);

        clCommandBuffer->executeCommands(cmdBuffersCount, pCommandBuffers);
    }

    PFN_vkVoidFunction clvkGetInstanceProcAddr(VkInstance instance, const char * pName) {
        std::printf("clvkGetInstanceProcAddr(0x%x, %s)\n", instance, pName);

        if (strcmp(pName, "vkCreateInstance") == 0) { return (PFN_vkVoidFunction) clvkCreateInstance; }
	    if (strcmp(pName, "vkEnumerateInstanceExtensionProperties") == 0) { return (PFN_vkVoidFunction) clvkEnumerateInstanceExtensionProperties; }
	    if (strcmp(pName, "vkEnumerateInstanceLayerProperties") == 0) { return (PFN_vkVoidFunction) clvkEnumerateInstanceLayerProperties; }
        
        if (instance) {
            auto vkcl = vkcl::Instance::getInstance(instance);
            
            return vkcl->getProcAddr(pName);
        }
        
        return NULL;
    }

    VkResult VKAPI_CALL vk_icdNegotiateLoaderICDInterfaceVersion(uint32_t* pSupportedVersion);
	PFN_vkVoidFunction VKAPI_CALL vk_icdGetInstanceProcAddr(VkInstance instance, const char* name);
    PFN_vkVoidFunction VKAPI_CALL vk_icdGetPhysicalDeviceProcAddr(VkInstance instance, const char* name);

    VkResult vk_icdNegotiateLoaderICDInterfaceVersion(uint32_t * pSupportedVersion) {
        if (pSupportedVersion && *pSupportedVersion >= 5) {
            *pSupportedVersion = 5;
            return VK_SUCCESS;
        }

        return VK_ERROR_INCOMPATIBLE_DRIVER;
    }

    PFN_vkVoidFunction vk_icdGetInstanceProcAddr(VkInstance instance, const char * pName) {        
        if (std::strcmp(pName, "vk_icdNegotiateLoaderICDInterfaceVersion") == 0) { return (PFN_vkVoidFunction) vk_icdNegotiateLoaderICDInterfaceVersion; }
	    if (std::strcmp(pName, "vk_icdGetPhysicalDeviceProcAddr") == 0) { return (PFN_vkVoidFunction) vk_icdGetPhysicalDeviceProcAddr; }
        
        return clvkGetInstanceProcAddr(instance, pName);
    }

    PFN_vkVoidFunction vk_icdGetPhysicalDeviceProcAddr(VkInstance instance, const char * pName) {
        return vk_icdGetInstanceProcAddr(instance, pName);
    }
}
