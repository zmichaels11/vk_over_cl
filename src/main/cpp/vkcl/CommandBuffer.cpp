#include "vkcl/CommandBuffer.hpp"

#include "vkcl/Commands.hpp"

namespace vkcl {
    VkResult CommandBuffer::begin(const VkCommandBufferBeginInfo * pInfo) noexcept {
        _payload.clear();
        _commands.clear();
        _recording = true;

        return VK_SUCCESS;
    }

    VkResult CommandBuffer::end() noexcept {
        _recording = false;

        return VK_SUCCESS;
    }

    VkResult CommandBuffer::reset(VkCommandBufferResetFlags flags) {
        _payload.clear();
        _commands.clear();
        _recording = false;

        return VK_SUCCESS;
    }

    void CommandBuffer::bindPipeline(
        VkPipelineBindPoint bindPoint, 
        VkPipeline pipeline) noexcept {

        auto cmd = std::make_shared<CmdBindPipeline> ();
        cmd->bindPoint = bindPoint;
        cmd->pipeline = pipeline;

        _commands.push_back(CommandType::BIND_PIPELINE);
        _payload.push_back(cmd);
    }

    void CommandBuffer::bindDescriptorSets(
        VkPipelineBindPoint bindPoint, 
        VkPipelineLayout layout, 
        uint32_t firstSet, uint32_t setCount, const VkDescriptorSet * pDescriptorSets, 
        uint32_t dynamicOffsetCount, const uint32_t * pDynamicOffsets) noexcept {

        auto cmd = std::make_shared<CmdBindDescriptorSets> ();
        cmd->bindPoint = bindPoint;
        cmd->layout = layout;
        cmd->firstSet = firstSet;
        cmd->setCount = setCount;
        cmd->pDescriptorSets = pDescriptorSets;
        cmd->dynamicOffsetCount = dynamicOffsetCount;
        cmd->pDynamicOffsets = pDynamicOffsets;        

        _commands.push_back(CommandType::BIND_DESCRIPTOR_SETS);
        _payload.push_back(cmd);
    }

    void CommandBuffer::dispatch(uint32_t x, uint32_t y, uint32_t z) noexcept {
        auto cmd = std::make_shared<CmdDispatch> ();
        cmd->x = x;
        cmd->y = y;
        cmd->z = z;

        _commands.push_back(CommandType::DISPATCH);
        _payload.push_back(cmd);
    }

    void CommandBuffer::dispatchIndirect(
        VkBuffer buffer, 
        VkDeviceSize offset) noexcept {

        auto cmd = std::make_shared<CmdDispatchIndirect> ();
        cmd->buffer = buffer;
        cmd->offset = offset;

        _commands.push_back(CommandType::DISPATCH_INDIRECT);
        _payload.push_back(cmd);
    }

    void CommandBuffer::copyBuffer(
        VkBuffer srcBuffer, VkBuffer dstBuffer, 
        uint32_t regionCount, const VkBufferCopy * pRegions) noexcept {

        auto cmd = std::make_shared<CmdCopyBuffer> ();
        cmd->srcBuffer = srcBuffer;
        cmd->dstBuffer = dstBuffer;
        cmd->regionCount = regionCount;
        cmd->pRegions = pRegions;

        _commands.push_back(CommandType::COPY_BUFFER);
        _payload.push_back(cmd);
    }

    void CommandBuffer::updateBuffer(
        VkBuffer dstBuffer, 
        VkDeviceSize dstOffset, 
        VkDeviceSize dataSize, 
        const void* pData) noexcept {

        auto cmd = std::make_shared<CmdUpdateBuffer> ();
        cmd->dstBuffer = dstBuffer;
        cmd->dstOffset = dstOffset;
        cmd->dataSize = dataSize;
        cmd->pData = pData;

        _commands.push_back(CommandType::UPDATE_BUFFER);
        _payload.push_back(cmd);
    }

    void CommandBuffer::fillBuffer(
        VkBuffer dstBuffer, 
        VkDeviceSize dstOffset, 
        VkDeviceSize size, 
        uint32_t data) noexcept {

        auto cmd = std::make_shared<CmdFillBuffer> ();
        cmd->dstBuffer = dstBuffer;
        cmd->dstOffset = dstOffset;
        cmd->size = size;
        cmd->data = data;

        _commands.push_back(CommandType::FILL_BUFFER);
        _payload.push_back(cmd);
    }

    void CommandBuffer::pipelineBarrier(
        VkPipelineStageFlags srcStageMask, 
        VkPipelineStageFlags dstStageMask, 
        VkDependencyFlags dependencyFlags, 
        uint32_t memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers, 
        uint32_t bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, 
        uint32_t imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers) noexcept {

        auto cmd = std::make_shared<CmdPipelineBarrier> ();
        cmd->srcStage = srcStageMask;
        cmd->dstStage = dstStageMask;
        cmd->dependencyFlags = dependencyFlags;
        cmd->memoryBarrierCount = memoryBarrierCount;
        cmd->pMemoryBarriers = pMemoryBarriers;
        cmd->bufferMemoryBarrierCount = bufferMemoryBarrierCount;
        cmd->pBufferMemoryBarriers = pBufferMemoryBarriers;
        cmd->imageMemoryBarrierCount = imageMemoryBarrierCount;
        cmd->pImageMemoryBarriers = pImageMemoryBarriers;

        _commands.push_back(CommandType::PIPELINE_BARRIER);
        _payload.push_back(cmd);
    }

    void CommandBuffer::pushConstants(
        VkPipelineLayout layout, 
        VkShaderStageFlags stageFlags, 
        uint32_t offset, 
        uint32_t size, 
        const void* pValues) noexcept {

        auto cmd = std::make_shared<CmdPushConstants> ();
        cmd->layout = layout;
        cmd->stageFlags = stageFlags;
        cmd->offset = offset;
        cmd->size = size;
        cmd->pValues = pValues;

        _commands.push_back(CommandType::PUSH_CONSTANT);
        _payload.push_back(cmd);
    }

    void CommandBuffer::executeCommands(
        uint32_t cmdBuffersCount, 
        const VkCommandBuffer* pCommandBuffers) noexcept {

        auto cmd = std::make_shared<CmdExecuteCommands> ();
        cmd->cmdBufferCount = cmdBuffersCount;
        cmd->pCommandBuffers = pCommandBuffers;

        _commands.push_back(CommandType::EXECUTE_COMMANDS);
        _payload.push_back(cmd);
    }
}