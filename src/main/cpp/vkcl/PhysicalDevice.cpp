#include "vkcl/PhysicalDevice.hpp"

#include <cstdio>
#include <cstring>

#include <algorithm>

#include <CL/cl.h>

namespace vkcl {
    PhysicalDevice::~PhysicalDevice() noexcept {

    }
    
    void PhysicalDevice::getProperties(VkPhysicalDeviceProperties * pProperties) noexcept {        
        pProperties->apiVersion = VK_MAKE_VERSION(1, 0, 0);
        pProperties->driverVersion = VK_MAKE_VERSION(0, 1, 0);
        pProperties->deviceID = 0xBEEF;

        {
            cl_uint vendorId;

            clGetDeviceInfo(_device, CL_DEVICE_VENDOR_ID, sizeof(cl_uint), &vendorId, nullptr);
            pProperties->vendorID = vendorId;
        }
        
        {
            cl_device_type type;

            clGetDeviceInfo(_device, CL_DEVICE_TYPE, sizeof(cl_device_type), &type, nullptr);

            switch (type) {
                case CL_DEVICE_TYPE_CPU:
                    pProperties->deviceType = VK_PHYSICAL_DEVICE_TYPE_CPU;
                    break;
                case CL_DEVICE_TYPE_GPU:
                    pProperties->deviceType = VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU;
                    break;
                default:
                    pProperties->deviceType = VK_PHYSICAL_DEVICE_TYPE_OTHER;
                    break;
            }
        }

        clGetDeviceInfo(_device, CL_DEVICE_NAME, 256, &pProperties->deviceName, nullptr);

        //TODO: best fit identify this. For now its zeroed out
        pProperties->limits = VkPhysicalDeviceLimits {};
        pProperties->sparseProperties = VkPhysicalDeviceSparseProperties {};

        pProperties->limits.maxMemoryAllocationCount = ~0;
        pProperties->limits.maxSamplerAllocationCount = ~0;
        pProperties->limits.nonCoherentAtomSize = 1;

        {
            cl_ulong globalMemSize;

            clGetDeviceInfo(_device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &globalMemSize, nullptr);

            pProperties->limits.maxStorageBufferRange = globalMemSize;
        }

        {
            cl_uint alignSize;
            cl_uint baseAlignSize;

            clGetDeviceInfo(_device, CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE, sizeof(cl_uint), &alignSize, nullptr);
            clGetDeviceInfo(_device, CL_DEVICE_MEM_BASE_ADDR_ALIGN, sizeof(cl_uint), &baseAlignSize, nullptr);

            pProperties->limits.minTexelBufferOffsetAlignment = alignSize;
            pProperties->limits.minUniformBufferOffsetAlignment = alignSize;
            pProperties->limits.bufferImageGranularity = baseAlignSize;
        }

        {
            cl_ulong maxConstantBufferSize;

            clGetDeviceInfo(_device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(cl_ulong), &maxConstantBufferSize, nullptr);

            pProperties->limits.maxUniformBufferRange = maxConstantBufferSize;
        }

        {
            cl_uint maxSamplerCount;
            cl_uint maxReadImages;
            cl_uint maxWriteImages;

            clGetDeviceInfo(_device, CL_DEVICE_MAX_SAMPLERS, sizeof(cl_uint), &maxSamplerCount, nullptr);
            clGetDeviceInfo(_device, CL_DEVICE_MAX_READ_IMAGE_ARGS, sizeof(cl_uint), &maxReadImages, nullptr);
            clGetDeviceInfo(_device, CL_DEVICE_MAX_WRITE_IMAGE_ARGS, sizeof(cl_uint), &maxWriteImages, nullptr);

            pProperties->limits.maxPerStageDescriptorSamplers = maxSamplerCount;
            pProperties->limits.maxPerStageDescriptorSampledImages = maxReadImages;
            pProperties->limits.maxPerStageDescriptorStorageImages = maxWriteImages;
        }

        {
            size_t maxWidth;

            clGetDeviceInfo(_device, CL_DEVICE_IMAGE_MAX_BUFFER_SIZE, sizeof(size_t), &maxWidth, nullptr);

            pProperties->limits.maxTexelBufferElements = maxWidth;
        }

        {
            size_t maxArrays;

            clGetDeviceInfo(_device, CL_DEVICE_IMAGE_MAX_ARRAY_SIZE, sizeof(size_t), &maxArrays, nullptr);

            pProperties->limits.maxImageArrayLayers = maxArrays;            
        }

        {
            size_t maxHeight, maxWidth;

            clGetDeviceInfo(_device, CL_DEVICE_IMAGE2D_MAX_WIDTH, sizeof(size_t), &maxWidth, nullptr); 
            clGetDeviceInfo(_device, CL_DEVICE_IMAGE2D_MAX_HEIGHT, sizeof(size_t), &maxHeight, nullptr);

            pProperties->limits.maxImageDimension2D = std::min(maxHeight, maxWidth);
        }

        {
            size_t maxWidth, maxHeight, maxDepth;

            clGetDeviceInfo(_device, CL_DEVICE_IMAGE3D_MAX_WIDTH, sizeof(size_t), &maxWidth, nullptr);
            clGetDeviceInfo(_device, CL_DEVICE_IMAGE3D_MAX_HEIGHT, sizeof(size_t), &maxHeight, nullptr);
            clGetDeviceInfo(_device, CL_DEVICE_IMAGE3D_MAX_DEPTH, sizeof(size_t), &maxDepth, nullptr);

            pProperties->limits.maxImageDimension3D = std::min(std::min(maxWidth, maxHeight), maxDepth);
        }

        {   
            size_t maxWorkgroupCount;

            clGetDeviceInfo(_device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &maxWorkgroupCount, nullptr);

            for (int i = 0; i < 3; i++) {
                pProperties->limits.maxComputeWorkGroupCount[i] = maxWorkgroupCount;
            }

            size_t maxWorkgroupDims;

            clGetDeviceInfo(_device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(size_t), &maxWorkgroupDims, nullptr);
            
            size_t maxItemSizes[maxWorkgroupDims];

            clGetDeviceInfo(_device, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(maxItemSizes), &maxItemSizes, nullptr);

            for (size_t i = 0; i < std::min(maxWorkgroupDims, static_cast<size_t> (3)); i++) {
                pProperties->limits.maxComputeWorkGroupSize[i] = maxItemSizes[i];
            }
        }
        

        static uint8_t GUID_BASE[] = {'D', 'E', 'A', 'D', 'B', 'E', 'E', 'F', 'C', 'A', 'F', 'E'};

        std::memcpy(pProperties->pipelineCacheUUID, GUID_BASE, 12);
        std::memcpy(pProperties->pipelineCacheUUID + 12, &pProperties->vendorID, 4);
    }

    void PhysicalDevice::getFeatures(VkPhysicalDeviceFeatures * pFeatures) noexcept {
        std::memset(pFeatures, 0, sizeof(VkPhysicalDeviceFeatures));
    }

    void PhysicalDevice::getFormatProperties(VkFormat format, VkFormatProperties * pFormatProperties) noexcept {
        
    }

    void PhysicalDevice::getQueueFamilyProperties(uint32_t* pQueueFamilyPropertyCount, VkQueueFamilyProperties* pQueueFamilyProperties) noexcept {
        if (nullptr == pQueueFamilyProperties) {
            *pQueueFamilyPropertyCount = 1;
        } else {
            pQueueFamilyProperties[0].minImageTransferGranularity.width = 1;
            pQueueFamilyProperties[0].minImageTransferGranularity.height = 1;
            pQueueFamilyProperties[0].minImageTransferGranularity.depth = 1;
            pQueueFamilyProperties[0].queueCount = 1;
            pQueueFamilyProperties[0].queueFlags = VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT;            
        }
    }

    void PhysicalDevice::getMemoryProperties(VkPhysicalDeviceMemoryProperties* pMemoryProperties) noexcept {
        pMemoryProperties->memoryHeapCount = 2;
        pMemoryProperties->memoryHeaps[0].flags = VK_MEMORY_HEAP_DEVICE_LOCAL_BIT;

        {
            cl_ulong deviceMemory;

            clGetDeviceInfo(_device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &deviceMemory, nullptr);
            pMemoryProperties->memoryHeaps[0].size = deviceMemory;
        }

        pMemoryProperties->memoryHeaps[1].flags = 0;
        pMemoryProperties->memoryHeaps[1].size = ~0;

        pMemoryProperties->memoryTypeCount = 2;
        pMemoryProperties->memoryTypes[0].heapIndex = 0;
        pMemoryProperties->memoryTypes[0].propertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

        pMemoryProperties->memoryTypes[1].heapIndex = 1;
        pMemoryProperties->memoryTypes[1].propertyFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
    }
}