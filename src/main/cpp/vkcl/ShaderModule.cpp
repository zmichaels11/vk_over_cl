#include "vkcl/ShaderModule.hpp"

#include "vkcl/Device.hpp"

namespace vkcl {
    ShaderModule::ShaderModule(Device * device, const VkShaderModuleCreateInfo * pInfo) {        
        _info = *pInfo;

        auto src = reinterpret_cast<const char *> (pInfo->pCode);
        cl_int err;

        _program = clCreateProgramWithSource(device->getCLContext(), 1, &src, nullptr, &err);

        if (CL_SUCCESS != err) {
            throw VK_ERROR_INITIALIZATION_FAILED;
        }
    }

    ShaderModule::~ShaderModule() noexcept {
        clReleaseProgram(_program);
    }
}