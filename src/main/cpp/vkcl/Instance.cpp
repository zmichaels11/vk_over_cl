#include "vkcl/Instance.hpp"

#include <cstdio>

#include <utility>

#include <CL/cl.h>

#include "vulkan.hpp"

#include "vkcl/PhysicalDevice.hpp"


namespace vkcl {
    void Instance::identifyPhysicalDevices() noexcept {
        cl_uint nPlatforms;
        
        clGetPlatformIDs(0, nullptr, &nPlatforms);

        auto platforms = std::vector<cl_platform_id> ();
        platforms.resize(nPlatforms);

        clGetPlatformIDs(nPlatforms, platforms.data(), nullptr);

        for (auto& platform : platforms) {
            cl_uint nDevices;

            clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, nullptr, &nDevices);

            auto devices = std::vector<cl_device_id> ();
            devices.resize(nDevices);

            clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, nDevices, devices.data(), nullptr);

            for (auto& device : devices) {
                cl_char string[10240] = {0};

                clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(string), &string, nullptr);

                _physicalDevices.push_back(std::make_unique<PhysicalDevice>(this, platform, device));
            }
        }        
    }

    void Instance::identifyInstanceProcs() noexcept {
        _procAddrMap["vkDestroyInstance"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestoryInstance);
        _procAddrMap["vkEnumeratePhysicalDevices"] = reinterpret_cast<PFN_vkVoidFunction> (clvkEnumeratePhysicalDevices);
        _procAddrMap["vkEnumerateInstanceExtensionProperties"] = reinterpret_cast<PFN_vkVoidFunction> (clvkEnumerateInstanceExtensionProperties);
        _procAddrMap["vkEnumerateDeviceExtensionProperties"] = reinterpret_cast<PFN_vkVoidFunction> (clvkEnumerateDeviceExtensionProperties);        
        _procAddrMap["vkEnumerateDeviceLayerProperties"] = reinterpret_cast<PFN_vkVoidFunction> (clvkEnumerateDeviceLayerProperties);
        _procAddrMap["vkGetPhysicalDeviceSparseImageFormatProperties"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetPhysicalDeviceSparseImageFormatProperties);
        _procAddrMap["vkGetPhysicalDeviceFeatures"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetPhysicalDeviceFeatures);
        _procAddrMap["vkGetPhysicalDeviceFormatProperties"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetPhysicalDeviceFormatProperties);
        _procAddrMap["vkGetPhysicalDeviceImageFormatProperties"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetPhysicalDeviceImageFormatProperties);
        _procAddrMap["vkGetPhysicalDeviceProperties"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetPhysicalDeviceProperties);
        _procAddrMap["vkGetPhysicalDeviceQueueFamilyProperties"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetPhysicalDeviceQueueFamilyProperties);
        _procAddrMap["vkGetPhysicalDeviceMemoryProperties"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetPhysicalDeviceMemoryProperties);
        _procAddrMap["vkGetInstanceProcAddr"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetInstanceProcAddr);
        _procAddrMap["vkGetDeviceProcAddr"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetDeviceProcAddr);
        _procAddrMap["vkCreateDevice"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateDevice);
        _procAddrMap["vkDestroyDevice"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyDevice);
        
        _procAddrMap["vkGetDeviceQueue"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetDeviceQueue);
        _procAddrMap["vkQueueSubmit"] = reinterpret_cast<PFN_vkVoidFunction> (clvkQueueSubmit);
        _procAddrMap["vkQueueWaitIdle"] = reinterpret_cast<PFN_vkVoidFunction> (clvkQueueWaitIdle);
        _procAddrMap["vkDeviceWaitIdle"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDeviceWaitIdle);
        _procAddrMap["vkAllocateMemory"] = reinterpret_cast<PFN_vkVoidFunction> (clvkAllocateMemory);
        _procAddrMap["vkFreeMemory"] = reinterpret_cast<PFN_vkVoidFunction> (clvkFreeMemory);
        _procAddrMap["vkMapMemory"] = reinterpret_cast<PFN_vkVoidFunction> (clvkMapMemory);
        _procAddrMap["vkUnmapMemory"] = reinterpret_cast<PFN_vkVoidFunction> (clvkUnmapMemory);
        _procAddrMap["vkFlushMappedMemoryRanges"] = reinterpret_cast<PFN_vkVoidFunction> (clvkFlushMappedMemoryRanges);
        _procAddrMap["vkFlushMappedMemoryRanges"] = reinterpret_cast<PFN_vkVoidFunction> (clvkFlushMappedMemoryRanges);
        _procAddrMap["vkInvalidateMappedMemoryRanges"] = reinterpret_cast<PFN_vkVoidFunction> (clvkInvalidateMappedMemoryRanges);
        _procAddrMap["vkGetDeviceMemoryCommitment"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetDeviceMemoryCommitment);
        _procAddrMap["vkBindBufferMemory"] = reinterpret_cast<PFN_vkVoidFunction> (clvkBindBufferMemory);
        _procAddrMap["vkBindImageMemory"] = reinterpret_cast<PFN_vkVoidFunction> (clvkBindImageMemory);
        _procAddrMap["vkGetBufferMemoryRequirements"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetBufferMemoryRequirements);
        _procAddrMap["vkGetImageMemoryRequirements"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetImageMemoryRequirements);
        _procAddrMap["vkGetImageSparseMemoryRequirements"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetImageSparseMemoryRequirements);
        _procAddrMap["vkQueueBindSparse"] = reinterpret_cast<PFN_vkVoidFunction> (clvkQueueBindSparse);
        _procAddrMap["vkCreateFence"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateFence);
        _procAddrMap["vkDestroyFence"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyFence);
        _procAddrMap["vkResetFences"] = reinterpret_cast<PFN_vkVoidFunction> (clvkResetFences);
        _procAddrMap["vkGetFenceStatus"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetFenceStatus);
        _procAddrMap["vkWaitForFences"] = reinterpret_cast<PFN_vkVoidFunction> (clvkWaitForFences);
        _procAddrMap["vkCreateSemaphore"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateSemaphore);
        _procAddrMap["vkDestroySemaphore"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroySemaphore);
        _procAddrMap["vkCreateEvent"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateEvent);
        _procAddrMap["vkDestroyEvent"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyEvent);
        _procAddrMap["vkGetEventStatus"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetEventStatus);
        _procAddrMap["vkSetEvent"] = reinterpret_cast<PFN_vkVoidFunction> (clvkSetEvent);
        _procAddrMap["vkResetEvent"] = reinterpret_cast<PFN_vkVoidFunction> (clvkResetEvent);
        _procAddrMap["vkCreateQueryPool"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateQueryPool);
        _procAddrMap["vkDestroyQueryPool"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyQueryPool);
        _procAddrMap["vkGetQueryPoolResults"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetQueryPoolResults);
        _procAddrMap["vkCreateBuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateBuffer);
        _procAddrMap["vkDestroyBuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyBuffer);
        _procAddrMap["vkCreateBufferView"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateBufferView);
        _procAddrMap["vkDestroyBufferView"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyBufferView);
        _procAddrMap["vkCreateImage"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateImage);
        _procAddrMap["vkDestroyImage"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyImage);
        _procAddrMap["vkGetImageSubresourceLayout"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetImageSubresourceLayout);
        _procAddrMap["vkCreateImageView"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateImageView);
        _procAddrMap["vkDestroyImageView"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyImageView);
        _procAddrMap["vkCreateShaderModule"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateShaderModule);
        _procAddrMap["vkDestroyShaderModule"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyShaderModule);
        _procAddrMap["vkCreatePipelineCache"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreatePipelineCache);
        _procAddrMap["vkDestroyPipelineCache"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyPipelineCache);
        _procAddrMap["vkGetPipelineCacheData"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetPipelineCacheData);
        _procAddrMap["vkMergePipelineCaches"] = reinterpret_cast<PFN_vkVoidFunction> (clvkMergePipelineCaches);
        _procAddrMap["vkCreateGraphicsPipelines"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateGraphicsPipelines);
        _procAddrMap["vkCreateComputePipelines"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateComputePipelines);
        _procAddrMap["vkDestroyPipeline"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyPipeline);
        _procAddrMap["vkCreatePipelineLayout"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreatePipelineLayout);
        _procAddrMap["vkDestroyPipelineLayout"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyPipelineLayout);
        _procAddrMap["vkCreateSampler"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateSampler);
        _procAddrMap["vkDestroySampler"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroySampler);
        _procAddrMap["vkCreateDescriptorSetLayout"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateDescriptorSetLayout);
        _procAddrMap["vkDestroyDescriptorSetLayout"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyDescriptorSetLayout);
        _procAddrMap["vkCreateDescriptorPool"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateDescriptorPool);
        _procAddrMap["vkDestroyDescriptorPool"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyDescriptorPool);
        _procAddrMap["vkResetDescriptorPool"] = reinterpret_cast<PFN_vkVoidFunction> (clvkResetDescriptorPool);
        _procAddrMap["vkAllocateDescriptorSets"] = reinterpret_cast<PFN_vkVoidFunction> (clvkAllocateDescriptorSets);
        _procAddrMap["vkFreeDescriptorSets"] = reinterpret_cast<PFN_vkVoidFunction> (clvkFreeDescriptorSets);
        _procAddrMap["vkUpdateDescriptorSets"] = reinterpret_cast<PFN_vkVoidFunction> (clvkUpdateDescriptorSets);
        _procAddrMap["vkCreateFramebuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateFramebuffer);
        _procAddrMap["vkDestroyFramebuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyFramebuffer);
        _procAddrMap["vkCreateRenderPass"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateRenderPass);
        _procAddrMap["vkDestroyRenderPass"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyRenderPass);
        _procAddrMap["vkGetRenderAreaGranularity"] = reinterpret_cast<PFN_vkVoidFunction> (clvkGetRenderAreaGranularity);
        _procAddrMap["vkCreateCommandPool"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCreateCommandPool);
        _procAddrMap["vkDestroyCommandPool"] = reinterpret_cast<PFN_vkVoidFunction> (clvkDestroyCommandPool);
        _procAddrMap["vkResetCommandPool"] = reinterpret_cast<PFN_vkVoidFunction> (clvkResetCommandPool);
        _procAddrMap["vkAllocateCommandBuffers"] = reinterpret_cast<PFN_vkVoidFunction> (clvkAllocateCommandBuffers);
        _procAddrMap["vkFreeCommandBuffers"] = reinterpret_cast<PFN_vkVoidFunction> (clvkFreeCommandBuffers);
        _procAddrMap["vkBeginCommandBuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkBeginCommandBuffer);
        _procAddrMap["vkEndCommandBuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkEndCommandBuffer);
        _procAddrMap["vkResetCommandBuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkResetCommandBuffer);
        _procAddrMap["vkCmdBindPipeline"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdBindPipeline);
        _procAddrMap["vkCmdSetViewport"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdSetViewport);
        _procAddrMap["vkCmdSetScissor"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdSetScissor);
        _procAddrMap["vkCmdSetLineWidth"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdSetLineWidth);
        _procAddrMap["vkCmdSetDepthBias"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdSetDepthBias);
        _procAddrMap["vkCmdSetBlendConstants"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdSetBlendConstants);
        _procAddrMap["vkCmdSetDepthBounds"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdSetDepthBounds);
        _procAddrMap["vkCmdSetStencilCompareMask"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdSetStencilCompareMask);
        _procAddrMap["vkCmdSetStencilWriteMask"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdSetStencilWriteMask);
        _procAddrMap["vkCmdSetStencilReference"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdSetStencilReference);
        _procAddrMap["vkCmdBindDescriptorSets"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdBindDescriptorSets);
        _procAddrMap["vkCmdBindIndexBuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdBindIndexBuffer);
        _procAddrMap["vkCmdBindVertexBuffers"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdBindVertexBuffers);
        _procAddrMap["vkCmdDraw"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdDraw);
        _procAddrMap["vkCmdDrawIndexed"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdDrawIndexed);
        _procAddrMap["vkCmdDrawIndirect"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdDrawIndirect);
        _procAddrMap["vkCmdDrawIndexedIndirect"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdDrawIndexedIndirect);
        _procAddrMap["vkCmdDispatch"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdDispatch);
        _procAddrMap["vkCmdDispatchIndirect"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdDispatchIndirect);
        _procAddrMap["vkCmdCopyBuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdCopyBuffer);
        _procAddrMap["vkCmdCopyImage"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdCopyImage);
        _procAddrMap["vkCmdBlitImage"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdBlitImage);
        _procAddrMap["vkCmdCopyBufferToImage"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdCopyBufferToImage);
        _procAddrMap["vkCmdCopyImageToBuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdCopyImageToBuffer);
        _procAddrMap["vkCmdUpdateBuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdUpdateBuffer);
        _procAddrMap["vkCmdFillBuffer"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdFillBuffer);
        _procAddrMap["vkCmdClearColorImage"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdClearColorImage);
        _procAddrMap["vkCmdClearDepthStencilImage"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdClearDepthStencilImage);
        _procAddrMap["vkCmdClearAttachments"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdClearAttachments);
        _procAddrMap["vkCmdResolveImage"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdResolveImage);
        _procAddrMap["vkCmdSetEvent"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdSetEvent);
        _procAddrMap["vkCmdResetEvent"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdResetEvent);
        _procAddrMap["vkCmdWaitEvents"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdWaitEvents);
        _procAddrMap["vkCmdPipelineBarrier"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdPipelineBarrier);
        _procAddrMap["vkCmdBeginQuery"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdBeginQuery);
        _procAddrMap["vkCmdEndQuery"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdEndQuery);
        _procAddrMap["vkCmdResetQueryPool"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdResetQueryPool);
        _procAddrMap["vkCmdWriteTimestamp"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdWriteTimestamp);
        _procAddrMap["vkCmdCopyQueryPoolResults"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdCopyQueryPoolResults);
        _procAddrMap["vkCmdPushConstants"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdPushConstants);
        _procAddrMap["vkCmdBeginRenderPass"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdBeginRenderPass);
        _procAddrMap["vkCmdNextSubpass"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdNextSubpass);
        _procAddrMap["vkCmdEndRenderPass"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdEndRenderPass);
        _procAddrMap["vkCmdExecuteCommands"] = reinterpret_cast<PFN_vkVoidFunction> (clvkCmdExecuteCommands);
    }

    Instance::Instance(const VkInstanceCreateInfo * pCreateInfo) noexcept {
        identifyInstanceProcs();
        identifyPhysicalDevices();
    }

    Instance::~Instance() noexcept {
        
    }

    VkResult Instance::getPhysicalDevices(uint32_t * pPhysicalDeviceCount, VkPhysicalDevice* pPhysicalDevices) noexcept {
        if (nullptr == pPhysicalDevices) {
            *pPhysicalDeviceCount = static_cast<uint32_t> (_physicalDevices.size());

            return VK_SUCCESS;
        } else {
            for (uint32_t i = 0; i < *pPhysicalDeviceCount; i++) {
                pPhysicalDevices[i] = _physicalDevices[i]->getVkPhysicalDevice();
            }

            return VK_SUCCESS;
        }
    }
}