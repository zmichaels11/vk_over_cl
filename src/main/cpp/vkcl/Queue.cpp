#include "vkcl/Queue.hpp"

namespace vkcl {
    VkResult Queue::waitIdle() noexcept {
        auto err = clFinish(_queue);

        switch (err) {
            case CL_INVALID_COMMAND_QUEUE:
                return VK_ERROR_DEVICE_LOST;
            case CL_OUT_OF_HOST_MEMORY:
                return VK_ERROR_OUT_OF_HOST_MEMORY;
            case CL_OUT_OF_RESOURCES:
                return VK_ERROR_OUT_OF_DEVICE_MEMORY;
            default:
                return VK_SUCCESS;
        }
    }
}