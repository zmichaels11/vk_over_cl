#include "vkcl/Buffer.hpp"

#include "vkcl/Device.hpp"
#include "vkcl/PhysicalDevice.hpp"
#include "vkcl/Memory.hpp"

#include <cstdio>

namespace vkcl {
    Buffer::~Buffer() noexcept {
        if (_isSubBuffer) {
            clReleaseMemObject(_buffer);
        }
    }

    void Buffer::getMemoryRequirements(VkMemoryRequirements * pMemoryRequirements) noexcept {
        auto pPhysicalDevice = _pDevice->getPhysicalDevice();
        std::size_t alignment = 1;
        
        clGetDeviceInfo(pPhysicalDevice->getCLDeviceID(), CL_DEVICE_MEM_BASE_ADDR_ALIGN, sizeof(std::size_t), &alignment, nullptr);
        alignment /= 8;
        
        pMemoryRequirements->size = _info.size;
        pMemoryRequirements->memoryTypeBits = 2;
        pMemoryRequirements->alignment = alignment;
    }

    VkResult Buffer::bindMemory(Memory * memory, VkDeviceSize offset) {
        if (0 == offset && _info.size == memory->getInfo().allocationSize) {
            _buffer = memory->getCLMemory();
            return VK_SUCCESS;
        } else {
            cl_int error;
            auto region = cl_buffer_region{};
            region.origin = offset;
            region.size = _info.size;

            _buffer = clCreateSubBuffer(memory->getCLMemory(), memory->getFlags(), CL_BUFFER_CREATE_TYPE_REGION, &region, &error);
            _isSubBuffer = true;

            switch (error) {
                case CL_SUCCESS:
                    return VK_SUCCESS;
                case CL_OUT_OF_RESOURCES:
                    return VK_ERROR_OUT_OF_DEVICE_MEMORY;
                case CL_OUT_OF_HOST_MEMORY:
                    return VK_ERROR_OUT_OF_HOST_MEMORY;
                default:
                    return VK_ERROR_INITIALIZATION_FAILED;
            }
        }
    }
}
