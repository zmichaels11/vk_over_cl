#include "vkcl/Device.hpp"

#include <cstdint>
#include <cstdio>

#include <utility>

#include "vkcl/PhysicalDevice.hpp"

namespace vkcl {
    Device::Device(
        PhysicalDevice * pPhysicalDevice, 
        const VkDeviceCreateInfo * pCreateInfo) {

        _physicalDevice = pPhysicalDevice;
        
        auto deviceId = pPhysicalDevice->getCLDeviceID();
        int err;

        _context = clCreateContext(0, 1, &deviceId, nullptr, nullptr, &err);

        switch (err) {
            case CL_SUCCESS:
                break;
            case CL_OUT_OF_HOST_MEMORY:
                throw VK_ERROR_OUT_OF_HOST_MEMORY;
                break;
            default:
                throw VK_ERROR_INITIALIZATION_FAILED;
                break;
        }

        _queues.push_back(std::make_unique<Queue>(clCreateCommandQueue(_context, deviceId, 0, &err)));

        switch (err) {
            case CL_SUCCESS:
                break;
            case CL_OUT_OF_HOST_MEMORY:
                throw VK_ERROR_OUT_OF_HOST_MEMORY;
                break;
            default:
                throw VK_ERROR_INITIALIZATION_FAILED;
                break;                    
        }

        _memoryQueue = clCreateCommandQueue(_context, deviceId, 0, &err);   

        switch (err) {
            case CL_SUCCESS:
                break;
            case CL_OUT_OF_HOST_MEMORY:
                throw VK_ERROR_OUT_OF_HOST_MEMORY;
            default:
                throw VK_ERROR_INITIALIZATION_FAILED;
                break;
        }
    }

    Device::~Device() noexcept {
        for (auto& queue : _queues) {
            clReleaseCommandQueue(queue->getCLQueue());
        }

        clReleaseContext(_context);
    }

    VkResult Device::waitIdle() noexcept {
        cl_int err = CL_SUCCESS;

        for (auto& queue : _queues) {
            err = queue->waitIdle();

            if (CL_SUCCESS != err) {
                break;
            }
        }

        if (CL_SUCCESS == err) {
            err = clFinish(_memoryQueue);
        }

        switch (err) {
            case CL_OUT_OF_HOST_MEMORY:
                return VK_ERROR_OUT_OF_HOST_MEMORY;
            case CL_OUT_OF_RESOURCES:
                return VK_ERROR_OUT_OF_DEVICE_MEMORY;
            case CL_INVALID_COMMAND_QUEUE:
                return VK_ERROR_DEVICE_LOST;
            default:
                return VK_SUCCESS;
        }
    }
}
