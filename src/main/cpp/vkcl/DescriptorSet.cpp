#include "vkcl/DescriptorSet.hpp"

#include "vkcl/Buffer.hpp"

namespace vkcl {
    void DescriptorSet::writeUpdate(const VkWriteDescriptorSet& write) noexcept {
        auto vkHandle = getVkDescriptorSet();

        switch (write.descriptorType) {
            case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER: {
                auto buffer = Buffer::getBuffer(write.pBufferInfo->buffer);
                auto mem = buffer->getCLBuffer();
                auto arg = ArgumentWrite{};
                arg.index = write.dstBinding;
                arg.size = sizeof(cl_mem);
                arg.value = &mem;
                
                auto it = _arguments.begin();
                bool insert = true;

                // replace the binding if it exists
                for (; it != _arguments.end(); it++) {
                    if (it->index == write.dstBinding) {
                        *it = arg;
                        insert = false;
                        break;
                    }
                }

                // otherwise insert it
                if (insert) {
                    _arguments.push_back(arg);
                }
            } break;
            default:
                break;            
        }
    }
}
