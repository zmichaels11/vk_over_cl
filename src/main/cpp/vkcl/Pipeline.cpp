#include "vkcl/Pipeline.hpp"

#include <cstdio>

#include <CL/cl.h>

#include "vkcl/Device.hpp"
#include "vkcl/PhysicalDevice.hpp"
#include "vkcl/ShaderModule.hpp"


namespace vkcl {
    Pipeline::Pipeline(Device * device, const VkComputePipelineCreateInfo * pInfo) {
        auto physicalDevice = device->getPhysicalDevice();
        auto clShaderModule = vkcl::ShaderModule::getShaderModule(pInfo->stage.module);
        auto program = clShaderModule->getCLProgram();
        auto err = clBuildProgram(program, 0, nullptr, nullptr, nullptr, nullptr);

        if (CL_SUCCESS != err) {
            std::size_t len;
            char buffer[4096];

            std::printf("Failed to compile program with error: %d\n", err);
            clGetProgramBuildInfo(program, physicalDevice->getCLDeviceID(), CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);

            std::printf("Build log: %s\n", buffer);
            
            throw VK_ERROR_INITIALIZATION_FAILED;
        }

        _kernel = clCreateKernel(program, pInfo->stage.pName, &err);

        if (CL_SUCCESS != err) {
            std::printf("ERR: %d\n", err);
            throw VK_ERROR_INITIALIZATION_FAILED;
        }
    }
}
