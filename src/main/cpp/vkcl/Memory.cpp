#include "vkcl/Memory.hpp"

#include "vkcl/Device.hpp"

namespace vkcl {
    Memory::Memory(Device * pDevice, const VkMemoryAllocateInfo * pCreateInfo) {
        _info = *pCreateInfo;

        _flags = CL_MEM_READ_WRITE;

        auto context = pDevice->getCLContext();
        int err;

        _memory = clCreateBuffer(
            context, 
            _flags,
            static_cast<size_t> (pCreateInfo->allocationSize),
            nullptr,
            &err);

        switch (err) {
            case CL_SUCCESS:
                break;
            case CL_OUT_OF_HOST_MEMORY:
                throw VK_ERROR_OUT_OF_HOST_MEMORY;
                break;
            case CL_OUT_OF_RESOURCES:
                throw VK_ERROR_OUT_OF_DEVICE_MEMORY;
                break;
            default:
                throw VK_ERROR_TOO_MANY_OBJECTS;
                break;
        }
    }

    Memory::~Memory() noexcept {
        clReleaseMemObject(_memory);        
    }
}
